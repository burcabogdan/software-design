﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SemesterProject.Models
{
    public class Comment
    {
        [Display(Name = "Article id")]
        [Range(100, 999)]
        [Required]
        public int ArticleId { get; set; }
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Comment id")]
        [Range(100, 999)]
        [Required]
        public int CommentId { get; set; }
        [Display(Name = "Commenter id")]
        [Range(100, 999)]
        [Required]
        public string CommenterId { get; set; }
        [Required]
        [Display(Name = "Content")]
        [StringLength(240, ErrorMessage = "The content must be at least 6 characters long.")]
        public string Content { get; set; }
        public bool IsFlagged { get; set; }

    }
}