﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SemesterProject
{
    public class PrimaryKeyException : Exception
    {
        public PrimaryKeyException()
        {
        }

        public PrimaryKeyException(string message) : base(message)
        {
        }

        public PrimaryKeyException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PrimaryKeyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}