namespace SemesterosProjectos.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Article",
                c => new
                    {
                        ArticleId = c.Int(nullable: false),
                        Content = c.String(nullable: false, maxLength: 700),
                        Writer = c.String(nullable: false),
                        IsFlagged = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ArticleId);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        CommentId = c.Int(nullable: false),
                        ArticleId = c.Int(nullable: false),
                        CommenterId = c.String(nullable: false),
                        Content = c.String(nullable: false, maxLength: 240),
                        IsFlagged = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId);
            
            CreateTable(
                "dbo.UserModel",
                c => new
                    {
                        UserName = c.String(nullable: false, maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false),
                        Password = c.String(maxLength: 100),
                        Role = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.UserName);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserModel");
            DropTable("dbo.Comment");
            DropTable("dbo.Article");
        }
    }
}
