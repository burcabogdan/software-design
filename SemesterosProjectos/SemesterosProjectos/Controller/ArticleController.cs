﻿using System.Linq;
using System.Web.Mvc;
using SemesterosProjectos.BusinessLayer;

namespace SemesterosProjectos.Controller
{
    public class ArticleController : System.Web.Mvc.Controller
    {
        public IArticleService ArticleService;

        public ArticleController()
        {
            ArticleService = new ArticleService();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var articles = ArticleService.GetArticles();
            return View(articles);
        }

        [HttpPost]
        public ActionResult Index(string author)
        {
            var articles = ArticleService.GetArticlesByAuthor(author).ToList();
            return View(articles);
        }

    }
}