﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.DTOs;
using BusinessLayer.Interfaces;
using DataLayer;
using DataLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class AccountService : IAccountService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IRepository<Account> _accountRepository;

        public AccountService()
        {
            _unitOfWork = new UnitOfWork();
            _accountRepository = _unitOfWork.AccountRepository;
        }

        public void AddAccount(AccountDto Account)
        {
            AutoMapper.Mapper.CreateMap<AccountDto, Account>();
            var AccountData = AutoMapper.Mapper.Map<AccountDto, Account>(Account);
            _accountRepository.Insert(AccountData);
            _unitOfWork.Save();

        }

        public void AddAccount(string name, int id, string password, bool isAdmin)
        {
            //DataLayer.Account currentAccountState = new DataLayer.Account
            //{
                
              
            //    name = name,
            //    idAccount = id,
            //    isAdmin = (byte)(isAdmin == true ? 1 : 0),
            //    password = password
            //};
            //_accountRepository.Insert(currentAccountState);
            //_unitOfWork.Save();
        }

        public AccountDto GetAccountById(int id)
        {
            Account Account = _accountRepository.GetById(id);
            AutoMapper.Mapper.CreateMap<Account, AccountDto>();
            var AccountDtos = AutoMapper.Mapper.Map<Account, AccountDto>(Account);
            return AccountDtos;
        }

        public void UpdateAccount(int SSC, int identityCard, string name, string address)
        {
            DataLayer.Account currentAccountState = new DataLayer.Account
            {
                idAccount = 1,
                idClient = 1,
                balance = 3333,
                
            };
            _accountRepository.Update(currentAccountState);
            _unitOfWork.Save();
        }

        public List<AccountDto> GetAllAccounts()
        {
            var Accounts = _accountRepository.Get();
            AutoMapper.Mapper.CreateMap<Account, AccountDto>();
            var AccountDtos = AutoMapper.Mapper.Map<List<DataLayer.Account>, List<AccountDto>>((List<Account>)Accounts);
            return AccountDtos;
        }

        public void RemoveAccount(int id)
        {
            _accountRepository.Delete(id);
            _unitOfWork.Save();
        }
    }
}
