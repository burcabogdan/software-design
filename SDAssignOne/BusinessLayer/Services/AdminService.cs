﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DTOs;
using BusinessLayer.Interfaces;
using DataLayer;
using DataLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class AdminService : IAdminService
    {
        private readonly UnitOfWork _unitOfWork ;
        private readonly IRepository<User> _userRepository;

        public AdminService()
        {
            _unitOfWork = new UnitOfWork();
            _userRepository = _unitOfWork.UserRepository;

        }

        public void AddUSer(UserDto user)
        {
                AutoMapper.Mapper.CreateMap<UserDto, User>();
                var userData = AutoMapper.Mapper.Map<UserDto, User>(user);
                _userRepository.Insert(userData);
                _unitOfWork.Save();
        }

        public void AddUSer(string name, int id, string password, bool isAdmin)
        {
            DataLayer.User currentUserState = new DataLayer.User
            {
                name = name,
                idUser = id,
                isAdmin = (byte)(isAdmin == true ? 1 : 0),
                password = password
            };
            _userRepository.Insert(currentUserState);
            _unitOfWork.Save();
        }

        public UserDto GetUserById(int id)
        {
            User user = _userRepository.GetById(id);
            AutoMapper.Mapper.CreateMap<User, UserDto>();
            var userDtos = AutoMapper.Mapper.Map<User, UserDto>(user);
            return userDtos;
        }

        public void UpdateUser(int idUser, string name, bool isAdmin, string password)
        {
            DataLayer.User currentUserState = new DataLayer.User
            {
                name = name,
                idUser = idUser,
                isAdmin = (byte) (isAdmin == true ? 1 : 0),
                password = password
            };
            _userRepository.Update(currentUserState);
            _unitOfWork.Save();
        }

        public List<UserDto> GetAllUsers()
        {
            var users = _userRepository.Get();
            AutoMapper.Mapper.CreateMap<User, UserDto>();
            var userDtos = AutoMapper.Mapper.Map < List<DataLayer.User>, List< UserDto>> ((List<User>)users);
            return userDtos;
        }

        public void RemoveUser(int id)
        {
            _userRepository.Delete(id);
            _unitOfWork.Save();
        }
    }
}
