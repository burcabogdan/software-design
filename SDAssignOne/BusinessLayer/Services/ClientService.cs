﻿using System.Collections.Generic;
using System.Data.Entity;
using BusinessLayer.DTOs;
using DataLayer;
using DataLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class ClientService : IClientService
    {
        #region Members
        private readonly UnitOfWork _unitOfWork;
        private readonly IRepository<Client> _clientRepository;

        #endregion

        public ClientService()
        {
            _unitOfWork = new UnitOfWork();
            _clientRepository = new UnitOfWork().ClientRepository;
        }

        public void AddClient(int SSC, int name, int identityCard, string address)
        {
        }

        public void AddClient(ClientDto client)
        {
            AutoMapper.Mapper.CreateMap<ClientDto, Client>();
            var clientData = AutoMapper.Mapper.Map<ClientDto, Client>(client);
            _clientRepository.Insert(clientData);
            //_unitOfWork.Context.Entry(clientData).State = EntityState.Modified;
            _unitOfWork.Save();
        }

        public ClientDto GetClientById(int id)
        {
            Client client = _clientRepository.GetById(id);
            AutoMapper.Mapper.CreateMap<Client, ClientDto>();
            var clientDto = AutoMapper.Mapper.Map<Client, ClientDto>(client);
            return clientDto;
        }

        public void UpdateClient(int SSC, int identityCard, string name, string address)
        {
            DataLayer.Client currentClientState = new DataLayer.Client
            {
                SSC = SSC,
                identityCard = identityCard,
                name = name,
                address = address,
                Account = null
            };
            _clientRepository.Update(currentClientState);
            _unitOfWork.Save();
        }

        public List<ClientDto> GetAllClients()
        {
            var clients = _clientRepository.Get();
            AutoMapper.Mapper.CreateMap<Client, ClientDto>();
            var clientDtos = AutoMapper.Mapper.Map<List<DataLayer.Client>, List<ClientDto>>((List<Client>)clients);
            return clientDtos;
        }

        public void RemoveClient(int id)
        {
            _clientRepository.Delete(id);
            _unitOfWork.Save();
        }
    }
}