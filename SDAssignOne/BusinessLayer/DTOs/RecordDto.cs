﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DTOs
{
    public class RecordDto
    {
        public int AccountSender { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public int AccountRecipient { get; set; }
        public int UserId { get; set; }
        public System.DateTime Date { get; set; }
        public int IdRecord { get; set; }

    }
}
