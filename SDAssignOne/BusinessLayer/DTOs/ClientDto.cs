﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.DTOs
{
    public class ClientDto
    {
        public int SSC { get; set; }
        public string Name { get; set; }
        public int IdentityCard { get; set; }
        public string Address { get; set; }

    }
}
