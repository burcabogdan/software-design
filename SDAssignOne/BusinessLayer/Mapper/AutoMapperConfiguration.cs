﻿namespace BusinessLayer.Mapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(x =>
            {
                x.AddProfile<AutoMapperMappings>();
                x.AddProfile<AutoMapperMappingsDtoToData>();
                x.AddProfile<AutoMapperMappingsAccountDomainData>();
                x.AddProfile<AutoMapperMappingsAccountDataDomain>();
                x.AddProfile<AutoMapperMappingsDtoToData>();

            });
        }
    }
}