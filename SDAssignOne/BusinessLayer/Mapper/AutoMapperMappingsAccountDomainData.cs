﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DTOs;
using DataLayer;

namespace BusinessLayer.Mapper
{
    public class AutoMapperMappingsAccountDomainData : Profile
    {
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<AccountDto, Account>()
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}
