﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DTOs;
using DataLayer;

namespace BusinessLayer.Mapper
{
    public class AutoMapperMappingsDtoToData: Profile
    { 
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<UserDto, User>().ForMember(dest => dest.isAdmin,
               opts => opts.MapFrom(
                   src => src.IsAdmin ? 1 : 0))
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}
