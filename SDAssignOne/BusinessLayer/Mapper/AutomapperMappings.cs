﻿using AutoMapper;
using BusinessLayer.DTOs;
using DataLayer;

namespace BusinessLayer.Mapper
{
    public class AutoMapperMappings : Profile
    {
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<User, UserDto>()
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}
