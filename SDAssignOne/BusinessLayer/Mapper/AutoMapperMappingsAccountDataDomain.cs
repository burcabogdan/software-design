﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.DTOs;
using DataLayer;

namespace BusinessLayer.Mapper
{
    public class AutoMapperMappingsAccountDataDomain : Profile
    {
        public override string ProfileName => "AutoMapperMappingsPresentation";

        protected override void Configure()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<Account, AccountDto>();
            });
        }
    }
}