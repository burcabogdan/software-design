﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.DTOs;

namespace BusinessLayer.Interfaces
{
    public interface IAccountService
    {
        void AddAccount(AccountDto user);
        AccountDto GetAccountById(int id);
        void UpdateAccount(int SSC, int identityCard, string name, string address);
        List<AccountDto> GetAllAccounts();
        void RemoveAccount(int id);
    }
}
