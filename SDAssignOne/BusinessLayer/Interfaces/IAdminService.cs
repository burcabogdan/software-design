﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.DTOs;

namespace BusinessLayer.Interfaces
{
    public interface IAdminService
    {
        void AddUSer(string name, int id, string password, bool isAdmin);
        void AddUSer(UserDto user);
        UserDto GetUserById(int id);
        void UpdateUser(int idUser, string name, bool isAdmin, string password);
        List<UserDto> GetAllUsers();
        void RemoveUser(int id);

    }
}
