﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDAssignOne.ViewModels
{
    public class ClientViewModel
    {
        public int SSC { get; set; }
        public string Name { get; set; }
        public int IdentityCard { get; set; }
        public string Address { get; set; }
    }
}