﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDAssignOne.ViewModels
{
    public class UserViewModel
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }

    }
}