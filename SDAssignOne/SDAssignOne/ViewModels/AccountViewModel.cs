﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDAssignOne.ViewModels
{
    public class AccountViewModel
    {
        public int IdAccount { get; set; }
        public decimal Balance { get; set; }
        public string Type { get; set; }
        public System.DateTime DateOfCreation { get; set; }
        public int? Interest { get; set; }
        public int IdClient { get; set; }
        public int? Fee { get; set; }
    }
}