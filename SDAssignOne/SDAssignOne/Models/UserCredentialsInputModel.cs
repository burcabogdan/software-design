﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SDAssignOne.Models
{
    public class UserCredentialsInputModel
    {
        [Required(ErrorMessage = "User id is Required")]
        [RegularExpression(@"/^(\d{5})$/;", ErrorMessage = "User Id must be 5 characters long.")]
        public int UserId { get; set; }

        [Required(ErrorMessage = "Password is Required")]
        public String Password { get; set; }

    }
}