﻿using AutoMapper;
using BusinessLayer.DTOs;
using SDAssignOne.ViewModels;

namespace SDAssignOne.Mapper
{
    public class AutoMapperMappings : Profile
    {
        public override string ProfileName => "AutoMapperMappingsPresentation";

        protected override void Configure()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<UserDto, UserViewModel>();
            });
        }
    }
}