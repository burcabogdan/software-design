﻿
namespace SDAssignOne.Mapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(x =>
            {
                x.AddProfile<AutoMapperMappingsViewModelToDto>();
                x.AddProfile<AutoMapperMappings>();
                x.AddProfile<AutoMapperMappingsClientDtoToViewModel>();
                x.AddProfile<AutoMapperMappingsClientViewModelToDto>();
                x.AddProfile<AutoMapperMappingsAccountDtoToViewModel>();
                x.AddProfile<AutoMapperMappingsAccountViewModelsToDto>();
                //   x.AddProfile<AutoMapperMappingsViewModelToDto>();
                //   x.AddProfile<AutoMapperMappingsViewModelToDto>();

            });
        }
    }
}