﻿using AutoMapper;
using BusinessLayer.DTOs;
using SDAssignOne.ViewModels;

namespace SDAssignOne.Mapper
{
    public class AutoMapperMappingsViewModelToDto : Profile
    {
        public override string ProfileName => "AutoMapperMappingsPresentationViewModelToDto";

        protected override void Configure()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<UserViewModel, UserDto>();
            });
        }
    }
}