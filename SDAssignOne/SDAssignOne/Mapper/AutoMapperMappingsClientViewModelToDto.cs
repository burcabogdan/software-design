﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using BusinessLayer.DTOs;
using SDAssignOne.ViewModels;

namespace SDAssignOne.Mapper
{
    public class AutoMapperMappingsClientViewModelToDto : Profile
    {
        public override string ProfileName => "AutoMapperMappingsPresentation";

        protected override void Configure()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<ClientViewModel, ClientDto>();
            });
        }
    }
}