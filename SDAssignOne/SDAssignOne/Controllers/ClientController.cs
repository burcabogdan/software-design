﻿using System.Collections.Generic;
using System.Web.Mvc;
using BusinessLayer.DTOs;
using BusinessLayer.Services;
using SDAssignOne.ViewModels;

namespace SDAssignOne.Controllers
{
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;

        public ClientController(IClientService clientService)
        {
            _clientService = clientService;
        }

        // GET: Client
        public ActionResult GetClients()
        {
            var clients = _clientService.GetAllClients();
            AutoMapper.Mapper.CreateMap<ClientDto, ClientViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<ClientDto>, List<ClientViewModel>>(clients);
            return View(clientViewModels);
        }

        [HttpGet]
        public ActionResult AddClient()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddClient([Bind(Include = "SSC, Name, IdentityCard, Address")]ClientViewModel Client)
        {

            AutoMapper.Mapper.CreateMap<ClientViewModel, ClientDto>();
            var ClientDto = AutoMapper.Mapper.Map<ClientViewModel, ClientDto>(Client);
            _clientService.AddClient(ClientDto);
            RedirectToAction("GetClients", "Client");
            return View();
        }

        [HttpGet]
        public ActionResult EditClient(int id)
        {
            var Client = _clientService.GetClientById(id);
            AutoMapper.Mapper.CreateMap<ClientDto, ClientViewModel>();
            var ClientViewModel = AutoMapper.Mapper.Map<ClientDto, ClientViewModel>(Client);
            return View("EditClient", ClientViewModel);
        }

        [HttpPost]
        public ActionResult EditClient(int SSC, int IdentityCard, string Name, string Address)
        {
            _clientService.UpdateClient(SSC, IdentityCard, Name, Address);
            RedirectToAction("GetClients", "Client");
            return View();
        }

        public ActionResult DeleteClient(int SSC)
        {
            _clientService.RemoveClient(SSC);
            return View("GetClients");
        }


    }
}