﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using BusinessLayer.DTOs;
using BusinessLayer.Interfaces;
using BusinessLayer.Services;
using SDAssignOne.ViewModels;

namespace SDAssignOne.Controllers
{
    public class UserController : Controller
    {
        private readonly IAdminService _adminService;

        public UserController(IAdminService adminService)
        {
            _adminService = adminService;
        }

        // GET: User
        public ActionResult GetUsers()
        {
            var users = _adminService.GetAllUsers();
            AutoMapper.Mapper.CreateMap<UserDto, UserViewModel>();
            var userViewModels = AutoMapper.Mapper.Map<List<UserDto>, List<UserViewModel>>(users);
            return View(userViewModels);
        }

        [HttpGet]
        public ActionResult AddUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddUser([Bind(Include = "IdUser, Name, Password, IsAdmin")]UserViewModel user)
        {

            AutoMapper.Mapper.CreateMap<UserViewModel, UserDto>();
            var userDto = AutoMapper.Mapper.Map<UserViewModel, UserDto>(user);
            _adminService.AddUSer(userDto);
            RedirectToAction("GetUsers", "User");
            return View();
        }

        [HttpGet]
        public ActionResult EditUser(int id)
        {
            var user = _adminService.GetUserById(id);
            AutoMapper.Mapper.CreateMap<UserDto, UserViewModel>();
            var userViewModel = AutoMapper.Mapper.Map<UserDto, UserViewModel>(user);
            return View("EditUser", userViewModel);
        }

        [HttpPost]
        public ActionResult EditUser(int IdUser, string Name, bool IsAdmin, string Password)
        {
            _adminService.UpdateUser(IdUser, Name, IsAdmin, Password);
            RedirectToAction("GetUsers", "User");
            return View();
        }

        public ActionResult DeleteUser(int id)
        {
            _adminService.RemoveUser(id);
            return View("GetUsers");
        }


    }
}