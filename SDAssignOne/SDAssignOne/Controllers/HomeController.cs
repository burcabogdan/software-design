﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLayer.Services;
using SDAssignOne.Models;
using SDAssignOne.ViewModels;

namespace SDAssignOne.Controllers
{
    public class HomeController : Controller
    {
        private readonly IClientService _clientService;

        public HomeController(IClientService clientService)
        {
            _clientService = clientService;
        }
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserCredentials(UserCredentialsInputModel userCredentials)
        {
            if (!ModelState.IsValid)
                return View("Index");

            UserCredentialsViewModel userCredentialsViewModel = new UserCredentialsViewModel
            {
                Password = userCredentials.Password,
                UserId = userCredentials.UserId
            };
            return View(userCredentialsViewModel);

        }
    }
}