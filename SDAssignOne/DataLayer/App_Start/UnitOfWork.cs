﻿using System;
using System.Data.Entity;
using DataLayer.Interfaces;
using DataLayer.Repositories;

namespace DataLayer
{
    public class UnitOfWork :  IDisposable
    {
        private readonly BankDbEntities _context = new BankDbEntities();
        private GenericRepository<Client> _clientRepository;
        private GenericRepository<Account> _accountRepository;
        private GenericRepository<User> _userRepository;


        public void Save()
        {
            //try
            //{
            _context.SaveChanges();
            //}catch(System.Data.Entity.Validation.DbEntityValidationException dbEx)
            //{
            //    Exception raise = dbEx;
            //    foreach (var validationErrors in dbEx.EntityValidationErrors)
            //    {
            //        foreach (var validationError in validationErrors.ValidationErrors)
            //        {
            //            string message = string.Format("{0}:{1}",
            //                validationErrors.Entry.Entity.ToString(),
            //                validationError.ErrorMessage);
            //            // raise a new exception nesting
            //            // the current instance as InnerException
            //            raise = new InvalidOperationException(message, raise);
            //        }
            //    }
            //    throw raise;
            //}
        }

        public DbContext Context
        {
            get { return _context; }
        }

        public GenericRepository<Client> ClientRepository
        {
            get
            {

                if (this._clientRepository == null)
                {
                    this._clientRepository = new GenericRepository<Client>(_context);
                }
                return _clientRepository;
            }
        }

        public GenericRepository<User> UserRepository
        {
            get
            {

                if (this._userRepository == null)
                {
                    this._userRepository = new GenericRepository<User>(_context);
                }
                return _userRepository;
            }
        }
        public GenericRepository<Account> AccountRepository
        {
            get
            {
                if (this._accountRepository == null)
                {
                    this._accountRepository = new GenericRepository<Account>(_context);
                }
                return _accountRepository;
            }
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}