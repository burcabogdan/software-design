
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/25/2016 00:13:51
-- Generated from EDMX file: E:\Software Design Git Real\SDAssignOne\DataLayer\BankDbModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [BankDb];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Account_Client]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Account] DROP CONSTRAINT [FK_Account_Client];
GO
IF OBJECT_ID(N'[dbo].[FK_Bill_Account]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Bill] DROP CONSTRAINT [FK_Bill_Account];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Account]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Account];
GO
IF OBJECT_ID(N'[dbo].[Bill]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Bill];
GO
IF OBJECT_ID(N'[dbo].[Client]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Client];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Accounts'
CREATE TABLE [dbo].[Accounts] (
    [idAccount] int  NOT NULL,
    [balance] decimal(19,4)  NOT NULL,
    [type] varchar(15)  NOT NULL,
    [dateOfCreation] datetime  NOT NULL,
    [interest] int  NULL,
    [idClient] int  NOT NULL,
    [fee] int  NULL
);
GO

-- Creating table 'Bills'
CREATE TABLE [dbo].[Bills] (
    [idAccount] int  NULL,
    [dateOfPayment] datetime  NOT NULL,
    [sum] decimal(19,4)  NOT NULL,
    [idBill] int  NOT NULL
);
GO

-- Creating table 'Clients'
CREATE TABLE [dbo].[Clients] (
    [SSC] int  NOT NULL,
    [name] varchar(50)  NOT NULL,
    [identityCard] int  NOT NULL,
    [address] varchar(80)  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [idUser] int  NOT NULL,
    [name] varchar(50)  NOT NULL,
    [password] varchar(10)  NOT NULL,
    [isAdmin] tinyint  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [idAccount] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [PK_Accounts]
    PRIMARY KEY CLUSTERED ([idAccount] ASC);
GO

-- Creating primary key on [idBill] in table 'Bills'
ALTER TABLE [dbo].[Bills]
ADD CONSTRAINT [PK_Bills]
    PRIMARY KEY CLUSTERED ([idBill] ASC);
GO

-- Creating primary key on [SSC] in table 'Clients'
ALTER TABLE [dbo].[Clients]
ADD CONSTRAINT [PK_Clients]
    PRIMARY KEY CLUSTERED ([SSC] ASC);
GO

-- Creating primary key on [idUser] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([idUser] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [idClient] in table 'Accounts'
ALTER TABLE [dbo].[Accounts]
ADD CONSTRAINT [FK_Account_Client]
    FOREIGN KEY ([idClient])
    REFERENCES [dbo].[Clients]
        ([SSC])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Account_Client'
CREATE INDEX [IX_FK_Account_Client]
ON [dbo].[Accounts]
    ([idClient]);
GO

-- Creating foreign key on [idAccount] in table 'Bills'
ALTER TABLE [dbo].[Bills]
ADD CONSTRAINT [FK_Bill_Account]
    FOREIGN KEY ([idAccount])
    REFERENCES [dbo].[Accounts]
        ([idAccount])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Bill_Account'
CREATE INDEX [IX_FK_Bill_Account]
ON [dbo].[Bills]
    ([idAccount]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------