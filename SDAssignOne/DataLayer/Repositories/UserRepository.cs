﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Core.Objects;
//using System.Data.Entity.Infrastructure;
//using System.Linq;
//using DataLayer.Interfaces;

//namespace DataLayer.Repositories
//{
//    public class UserRepository : IRepository<User>
//    {
//        private readonly BankDbEntities _entities = null;

//        public UserRepository(BankDbEntities entities)
//        {
//            this._entities = entities;
//        }

//        public void Add(User entity)
//        {
//            ObjectContext oc = ((IObjectContextAdapter)_entities).ObjectContext;
//            oc.AddObject("Users", entity);
//        }

//        public void Attach(User entity)
//        {
//            //entities.Users.Attach(entity);
//            //entities.s.ChangeObjectState(entity, EntityState.Modified);
//        }

//        public void Delete(User entity)
//        {
//            ObjectContext oc = ((IObjectContextAdapter)_entities).ObjectContext;
//            oc.DeleteObject(entity);
//        }

//        public User Get(Func<User, bool> predicate)
//        {
//            return _entities.Users.FirstOrDefault(predicate);
//        }

//        public IEnumerable<User> GetAll(Func<User, bool> predicate = null)
//        {
//            if (predicate != null)
//            {
//                return _entities.Users.Where(predicate);
//            }

//            return _entities.Users;
//        }
//    }
//}
