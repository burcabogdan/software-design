﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Assign1;
using Assign1.Business_Layer.Interfaces;
using Assign1.Business_Layer.Services;
using Assign1.Data_Layer.DataModels;
using Assign1.Data_Layer.Interfaces;
using Assign1.Data_Layer.Repositories;
using Assign1.Models;
using Microsoft.Practices.Unity;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shouldly;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        readonly Mock<GenericRepository<BankAccount>> _bankRepositoryMock = new Mock<GenericRepository<BankAccount>>();
        IUnityContainer myContainer = new UnityContainer();

        [TestMethod]
        public void TestMethod1()
        {
            var contextMock = new Mock<ApplicationDbContext>();

            contextMock.Setup(a => a.Set<BankAccount>()).Returns(Mock.Of<DbSet<BankAccount>>);
            IAccountService _accountService = new AccountService();

            var unitOfWorkMock = new Mock<UnitOfWork>();

            //  myContainer.RegisterType<IRepository<BankAccount>, GenericRepository<BankAccount>>();
            //    myContainer.RegisterInstance<IRepository<BankAccount>>(_bankRepositoryMock.Object);

            //Arrange
            var bankAccounts = new List<BankAccount>
            {
                new BankAccount {sscClient = "123-45-6789", Fee = 0, Interest = 5, IdAccount = 1, Balance = 5000, Type = "savings", DateOfCreation = new DateTime(2015, 12, 5, 14, 15, 30) },
                new BankAccount {sscClient = "123-45-6788", Fee = 0, Interest = 0, IdAccount = 2, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 15, 30) },
                new BankAccount {sscClient = "123-45-6788", Fee = 0, Interest = 0, IdAccount = 3, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 25, 30) },
                new BankAccount {sscClient = "123-45-6789", Fee = 0, Interest = 0, IdAccount = 4, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 35, 30) },
                new BankAccount {sscClient = "123-45-6786", Fee = 0, Interest = 0, IdAccount = 5, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 45, 30) }
            };
            var _bankRepositoryInstance = new GenericRepository<BankAccount>(contextMock.Object);
            unitOfWorkMock.Setup(x => x.AccountRepository).Returns(_bankRepositoryInstance);

            _bankRepositoryMock.Setup(m => m.Get(
                It.IsAny<Expression<Func<BankAccount, bool>>>(),
                It.IsAny<Func<IQueryable<BankAccount>, IOrderedQueryable<BankAccount>>>(),
                It.IsAny<string>())).Returns(bankAccounts);


            //Act 
            var result = _accountService.GetAllClientAccounts("123-45-6789");

            //Assert
       //     result.Count.ShouldBe(1);
        }

        [TestMethod]
        public void ExpectToWork()
        {
            var repositoryMock = new Mock<GenericRepository<BankAccount>>();
            var unitOfWork = new Mock<UnitOfWork>();
            IAccountService _accountService = new AccountService();
       //     unitOfWork.Setup(uow => uow.AccountRepository).Returns(repositoryMock);
            var account = new BankAccount
            {
                sscClient = "123-45-6789",
                Fee = 0,
                Interest = 5,
                IdAccount = 1,
                Balance = 5000,
                Type = "savings",
                DateOfCreation = new DateTime(2015, 12, 5, 14, 15, 30)
            };
            repositoryMock.Setup(i => i.GetById(It.IsAny<int>()))
                .Returns(account);

            var result = _accountService.GetAccountById(2);

            result.sscClient.ShouldBe(account.sscClient);
            repositoryMock.Verify();
        }
    }
}
