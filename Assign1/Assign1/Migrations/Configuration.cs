using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Text;
using Assign1.Data_Layer.DataModels;
using Assign1.Models;

namespace Assign1.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    //Here
    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
            ContextKey = "DefaultConnection";
        }
        //here
        protected override void Seed(ApplicationDbContext context)
        {
            var clients = new List<Client>
            {
                new Client {SSC = "123-45-6789", IdentityCard = 123459, Name = "Burca BogdanClient", Address = "Gheorghe Dima"},
                new Client {SSC = "123-45-6788", IdentityCard = 123458, Name = "Burca BogdanClientDoi", Address = "Gheorghe Dima"},
                new Client {SSC = "123-45-6787", IdentityCard = 123459, Name = "Burca BogdanClientTrei", Address = "Gheorghe Dima"},
                new Client {SSC = "123-45-6786", IdentityCard = 123457, Name = "Water Company", Address = "Gheorghe Dima"},
            };
            clients.ForEach(s => context.Client.AddOrUpdate(s));
            SaveChanges(context);
            var bankAccounts = new List<BankAccount>
            {
                new BankAccount {sscClient = "123-45-6789", Fee = 0, Interest = 5, IdAccount = 1, Balance = 5000, Type = "savings", DateOfCreation = new DateTime(2015, 12, 5, 14, 15, 30) },
                new BankAccount {sscClient = "123-45-6788", Fee = 0, Interest = 0, IdAccount = 2, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 15, 30) },
                new BankAccount {sscClient = "123-45-6788", Fee = 0, Interest = 0, IdAccount = 3, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 25, 30) },
                new BankAccount {sscClient = "123-45-6789", Fee = 0, Interest = 0, IdAccount = 4, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 35, 30) },
                new BankAccount {sscClient = "123-45-6786", Fee = 0, Interest = 0, IdAccount = 5, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 45, 30) }
            };
            bankAccounts.ForEach(s => context.BankAccount.AddOrUpdate(s));
            SaveChanges(context);
            // context.SaveChanges();
            //var records = new List<Record>
            //{
            //    new Record {AccountRecipient = "123-45-6786", AccountSender = "123-45-6789", UserId = "burcabogdan", IdRecord = 1, Amount = 500, Date = new DateTime(2016, 4, 5, 15, 45, 30), Type = "bill"},
            //    new Record {AccountRecipient = "123-45-6788", AccountSender = "123-45-6788", UserId = "burcabogdanAdmin", IdRecord = 2, Amount = 600, Date = new DateTime(2016, 5, 5, 15, 45, 30), Type = "transfer"},
            //    new Record {AccountRecipient = "123-45-6787", AccountSender = "123-45-6789", UserId = "burcabogdanRomulus", IdRecord = 3, Amount = 700, Date = new DateTime(2016, 6, 5, 15, 45, 30), Type = "transfer"}
            //};
            //records.ForEach(s => context.Records.AddOrUpdate(s));
            //SaveChanges(context);

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }

        private static void SaveChanges(DbContext context)
        {
            try
            {
                context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var sb = new StringBuilder();
                foreach (var failure in ex.EntityValidationErrors)
                {
                    sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                    foreach (var error in failure.ValidationErrors)
                    {
                        sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                        sb.AppendLine();
                    }
                }
                throw new DbEntityValidationException(
                    "Entity Validation Failed - errors follow:\n" +
                    sb.ToString(), ex
                );
            }
        }

    }


}
