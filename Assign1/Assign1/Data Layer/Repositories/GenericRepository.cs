﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using Assign1.Data_Layer.Interfaces;
using Assign1.Models;

namespace Assign1.Data_Layer.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : class

    {//here
        private ApplicationDbContext _entities;
        DbSet<TEntity> _dbSet;
        //hre
        public GenericRepository(ApplicationDbContext entities)
        {
            _entities = entities;
            _dbSet =  entities.Set<TEntity>();
        }

        public virtual IEnumerable<TEntity> Get(
            Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "")
        {
            IQueryable<TEntity> query = _dbSet;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            return orderBy != null ? orderBy(query).ToList() : query.ToList();
        }

        public virtual TEntity GetById(object id)
        {
            return _dbSet.Find(id);
        }

        public virtual void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
            _entities.SaveChanges();
        }

        public virtual void Delete(object id)
        {
            TEntity entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
            _entities.SaveChanges();
        }

        public virtual void Delete(TEntity entityToDelete)
        {
            if (_entities.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);
            _entities.SaveChanges();
        }

        public virtual void Update(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _entities.Entry(entityToUpdate).State = EntityState.Modified;
            _entities.SaveChanges();
        }

    }
}
