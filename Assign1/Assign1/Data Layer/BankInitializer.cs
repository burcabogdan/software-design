﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity.Migrations;
//using System.Linq;
//using System.Web;
//using Assign1.Data_Layer.DataModels;
//using Assign1.Models;

//namespace Assign1.Data_Layer
//{//here
//    public class BankInitializer : DbMigrationsConfiguration<ApplicationDbContext> // System.Data.Entity.CreateDatabaseIfNotExists<BankContext>
//    {
//        public BankInitializer()
//        {
//            AutomaticMigrationsEnabled = true;
//        }
//        //here
//        protected override void Seed(ApplicationDbContext context)
//        {
//            var clients = new List<Client>
//            {
//                new Client {SSC = "123-45-6789", IdentityCard = 123457, Name = "Burca BogdanClient", Address = "Gheorghe Dima"},
//                new Client {SSC = "123-45-6788", IdentityCard = 123456, Name = "Burca BogdanClient1", Address = "Gheorghe Dima"},
//                new Client {SSC = "123-45-6787", IdentityCard = 123456, Name = "Burca BogdanClient2", Address = "Gheorghe Dima"},
//                new Client {SSC = "123-45-6786", IdentityCard = 123456, Name = "Water Company", Address = "Gheorghe Dima"},
//            };
//            clients.ForEach(s => context.Client.Add(s));
//            context.SaveChanges();
//            var bankAccounts = new List<BankAccount>
//            {
//                new BankAccount {sscClient = "123-45-6789", Fee = 0, Interest = 5, IdAccount = 1, Balance = 5000, Type = "savings", DateOfCreation = new DateTime(2015, 12, 5, 14, 15, 30) },
//                new BankAccount {sscClient = "123-45-6788", Fee = 0, Interest = 0, IdAccount = 2, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 15, 30) },
//                new BankAccount {sscClient = "123-45-6788", Fee = 0, Interest = 0, IdAccount = 3, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 25, 30) },
//                new BankAccount {sscClient = "123-45-6789", Fee = 0, Interest = 0, IdAccount = 4, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 35, 30) },
//                new BankAccount {sscClient = "123-45-6786", Fee = 0, Interest = 0, IdAccount = 5, Balance = 10000, Type = "spending", DateOfCreation = new DateTime(2015, 12, 5, 15, 45, 30) }
//            };
//            bankAccounts.ForEach(s => context.BankAccount.Add(s));
//            context.SaveChanges();
//            var records = new List<Record>
//            {
//                new Record {AccountRecipient = "123-45-6786", AccountSender = "123-45-6789", UserId = "burcabogdan", IdRecord = 1, Amount = 500, Date = new DateTime(2016, 4, 5, 15, 45, 30), Type = "bill"},
//                new Record {AccountRecipient = "123-45-6788", AccountSender = "123-45-6788", UserId = "burcabogdanAdmin", IdRecord = 2, Amount = 600, Date = new DateTime(2016, 5, 5, 15, 45, 30), Type = "transfer"},
//                new Record {AccountRecipient = "123-45-6787", AccountSender = "123-45-6789", UserId = "burcabogdanRomulus", IdRecord = 3, Amount = 700, Date = new DateTime(2016, 6, 5, 15, 45, 30), Type = "transfer"}
//            };
//            records.ForEach(s => context.Records.Add(s));
//            context.SaveChanges();
//        }
//    }
//}