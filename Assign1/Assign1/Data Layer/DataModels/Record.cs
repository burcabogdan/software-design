﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Assign1.Models;

namespace Assign1.Data_Layer.DataModels
{
    public class Record
    {
        public string AccountSender { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public string AccountRecipient { get; set; }
        public string UserId { get; set; }
        public System.DateTime Date { get; set; }
        [Key]
        public int IdRecord { get; set; }

        public virtual BankAccount Account { get; set; }
        public virtual ApplicationUser User { get; set; }
    }
}