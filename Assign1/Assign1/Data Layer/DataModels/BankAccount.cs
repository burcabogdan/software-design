﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Assign1.Data_Layer.DataModels
{
    public sealed class BankAccount
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Key]
        public int IdAccount { get; set; }
        public decimal Balance { get; set; }
        public string Type { get; set; }
        public System.DateTime? DateOfCreation { get; set; }
        public int? Interest { get; set; }
        public string sscClient { get; set; }
        public int? Fee { get; set; }
   //     public Client Client { get; set; }
//        public ICollection<Record> Records { get; set; }

    }
}