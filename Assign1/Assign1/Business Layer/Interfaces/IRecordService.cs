﻿using System;
using System.Collections.Generic;
using Assign1.Business_Layer.DTOs;
using Assign1.Data_Layer.DataModels;

namespace Assign1.Business_Layer.Interfaces
{
    public interface IRecordService
    {
        void AddReport(RecordDto recordDto);
        List<RecordDto> GetReportsByUser(string userId, DateTime start, DateTime end);
        List<RecordDto> GetReports();
        int GetNewRecordId();
        List<RecordDto> SearchRecords(DateTime? StartDateTime, DateTime? EndDateTime, string UserName);
    }
}
