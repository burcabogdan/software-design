﻿using System.Collections.Generic;
using Assign1.Business_Layer.DTOs;
using Assign1.Models;

namespace Assign1.Business_Layer.Interfaces
{
    public interface IAdminService
    {
        void AddUSer(string name, int id, string password, bool isAdmin);
        void AddUSer(UserDto user);
        ApplicationUser GetUserById(string id);
        ApplicationUser UpdateUser(ApplicationUser user);
        List<ApplicationUser> GetAllUsers();
        void RemoveUser(string id);

    }
}
