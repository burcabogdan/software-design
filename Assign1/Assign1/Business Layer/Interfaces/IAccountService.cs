﻿using System.Collections.Generic;
using Assign1.Business_Layer.DTOs;

namespace Assign1.Business_Layer.Interfaces
{
    public interface IAccountService
    {
        void AddAccount(BankAccountDto user);
        BankAccountDto GetAccountById(int id);
        void UpdateAccount(BankAccountDto accountDto);
        List<BankAccountDto> GetAllAccounts();
        List<BankAccountDto> GetAllClientAccounts(string id);
        void RemoveAccount(int id);
        void PerformTranscation(int sourceId, int destinationId, int sum, string type, string id);

    }
}
