﻿using System.Collections.Generic;
using Assign1.Business_Layer.DTOs;

namespace Assign1.Business_Layer.Interfaces
{
    public interface IClientService
    {
        void AddClient(string SSC, int name, int identityCard, string address);
        void AddClient(ClientDto user);
        ClientDto GetClientById(string id);
        void UpdateClient(string SSC, int identityCard, string name, string address);
        List<ClientDto> GetAllClients();
        void RemoveClient(string SSC);
    }
}