﻿using System;

namespace Assign1.Business_Layer.DTOs
{
    public class BankAccountDto
    {
        public int IdAccount { get; set; }
        public decimal Balance { get; set; }
        public string Type { get; set; }
        public System.DateTime? DateOfCreation { get; set; }
        public int? Interest { get; set; }
        public string sscClient { get; set; }
        public int? Fee { get; set; }
    }
}
