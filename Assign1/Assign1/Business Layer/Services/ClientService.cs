﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Data_Layer.DataModels;
using Assign1.Data_Layer.Interfaces;

namespace Assign1.Business_Layer.Services
{
    [HandleError]
    public class ClientService : IClientService
    {
        #region Members
        private readonly UnitOfWork _unitOfWork;
        private readonly IRepository<Client> _clientRepository;

        #endregion

        public ClientService()
        {
            _unitOfWork = new UnitOfWork();
            _clientRepository = new UnitOfWork().ClientRepository;
        }

        public void AddClient(string SSC, int name, int identityCard, string address)
        {
        }

        public void AddClient(ClientDto client)
        {
            AutoMapper.Mapper.CreateMap<ClientDto, Client>();
            var clientData = AutoMapper.Mapper.Map<ClientDto, Client>(client);
            var test = _clientRepository.GetById(client.SSC);
            if (test != null)
                if(test.IdentityCard == client.IdentityCard || test.SSC == client.SSC)
                throw new PrimaryKeyException();
            _clientRepository.Insert(clientData);
            _unitOfWork.Context.Entry(clientData).State = EntityState.Modified;
            _unitOfWork.Save();
        }

        public ClientDto GetClientById(string id)
        {
            Client client = _clientRepository.GetById(id);
            AutoMapper.Mapper.CreateMap<Client, ClientDto>();
            var clientDto = AutoMapper.Mapper.Map<Client, ClientDto>(client);
            return clientDto;
        }

        public void UpdateClient(string SSC, int identityCard, string name, string address)
        {
            var client = _clientRepository.GetById(SSC);
            client.Name = name;
            client.Address = address;
            _clientRepository.Update(client);
            _unitOfWork.Save();
        }

        public List<ClientDto> GetAllClients()
        {
            var clients = _clientRepository.Get();
            AutoMapper.Mapper.CreateMap<Client, ClientDto>();
            var clientDtos = AutoMapper.Mapper.Map<List<Data_Layer.DataModels.Client>, List<ClientDto>>((List<Client>)clients);
            return clientDtos;
        }

        public void RemoveClient(string SSC)
        {
            _clientRepository.Delete(SSC);
            _unitOfWork.Save();
        }
    }
}