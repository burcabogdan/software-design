﻿using System.Collections.Generic;
using System.Linq;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Data_Layer.Interfaces;
using Assign1.Models;

namespace Assign1.Business_Layer.Services
{
    public class AdminService : IAdminService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IRepository<ApplicationUser> _userRepository;

        public AdminService()
        {
            _unitOfWork = new UnitOfWork();
            _userRepository = _unitOfWork.UserRepository;

        }

        public void AddUSer(string name, int id, string password, bool isAdmin)
        {
            throw new System.NotImplementedException();
        }

        public void AddUSer(UserDto user)
        {
            //AutoMapper.Mapper.CreateMap<UserDto, User>();
            //var userData = AutoMapper.Mapper.Map<UserDto, User>(user);
            //_userRepository.Insert(userData);
            //_unitOfWork.Save();
        }


        public ApplicationUser GetUserById(string id)
        {
            ApplicationUser user = _userRepository.GetById(id);
           // var user = Db.Users.First(u => u.Id == id);
            return user;
        }

        public ApplicationUser UpdateUser(ApplicationUser userApp)
        {
            var user = _userRepository.Get().First(u => u.UserName == userApp.UserName);
            
            user.IsAdmin = userApp.IsAdmin;
            user.Name = userApp.Name;
            if(_userRepository.Get().Where(x => x.Email == userApp.Email).Count() == 0)
                user.Email = userApp.Email;
            _userRepository.Update(user);
            _unitOfWork.Save();
            return _userRepository.Get().First(u => u.UserName == userApp.UserName);
        }

        public List<ApplicationUser> GetAllUsers()
        {
            var users = _userRepository.Get();
            return users.ToList();
        }

        public void RemoveUser(string id)
        {
            //var Db = new ApplicationDbContext();
            //var userAccount = Db.Users.First(u => u.Id == id);
            //Db.Users.Remove(userAccount);
            //Db.SaveChanges();
            _userRepository.Delete(id);
            _unitOfWork.Save();
        }
    }
}
