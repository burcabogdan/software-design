﻿using System.Collections.Generic;
using System.Web.Mvc;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Models;

namespace Assign1.Controllers
{
    [HandleError]
    [Authorize]
    public class ClientController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IAccountService _accountService;

        public ClientController(IClientService clientService, IAccountService accountService)
        {
            _clientService = clientService;
            _accountService = accountService;
        }

        // GET: Client
        public ActionResult GetClients()
        {
            var clients = _clientService.GetAllClients();
            AutoMapper.Mapper.CreateMap<ClientDto, ClientViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<ClientDto>, List<ClientViewModel>>(clients);
            return View(clientViewModels);
        }

        [HttpGet]
        public ActionResult AddClient()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddClient([Bind(Include = "SSC, Name, IdentityCard, Address")]ClientViewModel Client)
        {

            try
            {
                AutoMapper.Mapper.CreateMap<ClientViewModel, ClientDto>();
                var ClientDto = AutoMapper.Mapper.Map<ClientViewModel, ClientDto>(Client);
                _clientService.AddClient(ClientDto);
                RedirectToAction("GetClients", "Client");
                return View();

            }
            catch (PrimaryKeyException p)
            {
                return View(Client);
            }
        }

        [HttpGet]
        public ActionResult EditClient(string id)
        {
            var Client = _clientService.GetClientById(id);
            AutoMapper.Mapper.CreateMap<ClientDto, ClientViewModel>();
            var ClientViewModel = AutoMapper.Mapper.Map<ClientDto, ClientViewModel>(Client);
            _accountService.GetAllClientAccounts(id);

            return View("EditClient", ClientViewModel);
        }

        [HttpPost]
        public ActionResult EditClient(string SSC, int IdentityCard, string Name, string Address)
        {
            _clientService.UpdateClient(SSC, IdentityCard, Name, Address);
            return View("EditClient", new ClientViewModel
            {
                SSC = SSC,
                IdentityCard = IdentityCard,
                Name = Name,
                Address = Address
            });
        }

        public ActionResult DeleteClient(string id)
        {
            _clientService.RemoveClient(id);
            var clients = _clientService.GetAllClients();
            AutoMapper.Mapper.CreateMap<ClientDto, ClientViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<ClientDto>, List<ClientViewModel>>(clients);
            return View("GetClients", clientViewModels);
        }

        public ActionResult GetAllClientAccounts(string id)
        {
            var bankAccounts = _accountService.GetAllClientAccounts(id);
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccountViewModel>();
            var bankAccountsViewModels = AutoMapper.Mapper.Map<List<BankAccountDto>, List<BankAccountViewModel>>(bankAccounts);
            return View("~/Views/BankAccount/Index.cshtml", bankAccountsViewModels);
        }
    }
}