﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Models;

namespace Assign1.Controllers
{
    [HandleError]
    [Authorize(Roles = "SuperAdmin")]
    public class RecordController : Controller
    {
        private readonly IClientService _clientService;
        private readonly IAccountService _accountService;
        private readonly IAdminService _adminService;
        private readonly IRecordService _recordService;

        public RecordController(IClientService clientService, IAccountService accountService, IRecordService recordService, IAdminService adminService)
        {
            _clientService = clientService;
            _accountService = accountService;
            _recordService = recordService;
            _adminService = adminService;
        }

        // GET: Record
        public ActionResult Index(string userId)
        {
            var Db = new ApplicationDbContext();
            var user = Db.Users.First();
            var clients = _recordService.GetReportsByUser(user.Id, DateTime.MinValue, DateTime.MaxValue);
            AutoMapper.Mapper.CreateMap<RecordDto, RecordViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<RecordDto>, List<RecordViewModel>>(clients);
            return View( new RecordViewModelSearch { RecordViewModels = clientViewModels, UserNames = new List<string>()});
        }

        // GET: Record
        public ActionResult IndexAll()
        {
            var clients = _recordService.GetReports();
            AutoMapper.Mapper.CreateMap<RecordDto, RecordViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<RecordDto>, List<RecordViewModel>>(clients);
            var userNames = _adminService.GetAllUsers();
            List<string> usernamesList = new List<string>();
            foreach (var clientss in clients)
            {
                if(clientss.UserId != String.Empty)
                usernamesList.Add(userNames.Where(x => x.Id == clientss.UserId).FirstOrDefault().UserName);
            }
            return View("Index", new RecordViewModelSearch {RecordViewModels = clientViewModels, UserNames = usernamesList});
        }

        public ActionResult SearchRecords(DateTime? StartDateTime, DateTime? EndDateTime, List<String> UserNames)
        {
            var clients = _recordService.SearchRecords(StartDateTime, EndDateTime, UserNames.FirstOrDefault());
            AutoMapper.Mapper.CreateMap<RecordDto, RecordViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<RecordDto>, List<RecordViewModel>>(clients);
            var userNames = _adminService.GetAllUsers().Select(x => x.UserName).ToList();
            return View("Index", new RecordViewModelSearch
            {
                RecordViewModels = clientViewModels,
                UserNames = userNames
            });
        }

    }
}