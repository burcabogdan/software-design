﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assign1.Models
{
    public class RecordViewModelSearch
    {
        public List<RecordViewModel> RecordViewModels { get; set; }
        public DateTime? StartDateTime { get; set; }
        public DateTime? EndDateTime { get; set; }
        public List<String> UserNames { get; set; }
        public List<String> CurrentUserChosen { get; set; } 
    }
}