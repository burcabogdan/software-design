﻿using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.Ajax.Utilities;

namespace Assign1.Models
{
    public class BankAccountViewModel
    {
        [Required]
        [Range(0.0, Double.MaxValue)]
        public int IdAccount { get; set; }
        [Required]
        [Range(0.0, Double.MaxValue)]
        public decimal Balance { get; set; }
        [Required]
        [RegularExpression(@"\b(spending|savings)\b", ErrorMessage = "Type must be either spending or savings accounts.")]
        public string Type { get; set; }
        [Required]
        public System.DateTime? DateOfCreation { get; set; }
        [Range(0.0, Double.MaxValue)]
        public int? Interest { get; set; }
        [Required(ErrorMessage = "{0} is a required field.")]
        [DataType(DataType.Text)]
        [Display(Name = "Social Security Number")]
        [RegularExpression(@"^(\d{3}-?\d{2}-?\d{4}|XXX-XX-XXXX)$", ErrorMessage = "{0} is required and must be of XXX-XX-XXXX format.")]
        public string sscClient { get; set; }
        [Required]
        [Range(0.0, Double.MaxValue)]
        public int? Fee { get; set; }
    }
}