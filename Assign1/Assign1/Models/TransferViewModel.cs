﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Assign1.Models
{
    public class TransferViewModel
    {
        public int AccountSender { get; set; }
        public int AccountRecipient { get; set; }
        public string Type { get; set; }
        [Required]
        [Range(0.0, Double.MaxValue)]
        public int Amount { get; set; }
        public List<int> AccountsList { get; set; }
    }
}