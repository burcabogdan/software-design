﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assign1.Business_Layer.DTOs;
using Assign1.Models;
using AutoMapper;

namespace Assign1.Mapper
{
    public class AutoMapperMappingsRecordDtoToViewModel : Profile
    {
        public override string ProfileName => "AutoMapperMappingsPresentation";

        protected override void Configure()
        {
            var config = new MapperConfiguration(x =>
            {
                x.CreateMap<RecordDto, RecordViewModel>();
            });
        }
    }

}