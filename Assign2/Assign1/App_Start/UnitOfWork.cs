﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Text;
using Assign1.Data_Layer;
using Assign1.Data_Layer.DataModels;
using Assign1.Data_Layer.Repositories;
using Assign1.Models;

namespace Assign1
{
    public class UnitOfWork :  IDisposable
    {
        //here
        private readonly ApplicationDbContext _context = new ApplicationDbContext();
        private GenericRepository<Client> _clientRepository;
        private GenericRepository<BankAccount> _accountRepository;
        private GenericRepository<Record> _recordRepository;
        private GenericRepository<ApplicationUser> _userRepository;


        public void Save()
        {
            try
            {
            _context.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbValEx)
            {
                var outputLines = new StringBuilder();
                foreach (var eve in dbValEx.EntityValidationErrors)
                {
                    outputLines.AppendFormat("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:"
                      , DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.AppendFormat("- Property: \"{0}\", Error: \"{1}\""
                         , ve.PropertyName, ve.ErrorMessage);
                    }
                }

                throw new DbEntityValidationException(string.Format("Validation errors\r\n{0}"
                 , outputLines.ToString()), dbValEx);

            }
        }

        public DbContext Context
        {
            get { return _context; }
        }
        public GenericRepository<Record> RecordRepository
        {
            get
            {

                if (this._recordRepository == null)
                {
                    this._recordRepository = new GenericRepository<Record>(_context);
                }
                return _recordRepository;
            }
        }

        public GenericRepository<Client> ClientRepository
        {
            get
            {

                if (this._clientRepository == null)
                {
                    this._clientRepository = new GenericRepository<Client>(_context);
                }
                return _clientRepository;
            }
        }

        public GenericRepository<ApplicationUser> UserRepository
        {
            get
            {

                if (this._recordRepository == null)
                {
                    this._userRepository = new GenericRepository<ApplicationUser>(_context);
                }
                return _userRepository;
            }
        }
        public GenericRepository<BankAccount> AccountRepository
        {
            get
            {
                if (this._accountRepository == null)
                {
                    this._accountRepository = new GenericRepository<BankAccount>(_context);
                }
                return _accountRepository;
            }
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}