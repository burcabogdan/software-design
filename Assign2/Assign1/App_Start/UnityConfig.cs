using System;
using System.Data.Entity;
using System.Web;
using Assign1.Business_Layer.Interfaces;
using Assign1.Business_Layer.Services;
using Assign1.Controllers;
using Assign1.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using Microsoft.Practices.Unity;

namespace Assign1
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<DbContext, ApplicationDbContext>(new HierarchicalLifetimeManager());
            container.RegisterType<UserManager<ApplicationUser>>(
                new HierarchicalLifetimeManager());
            container.RegisterType<AccountController>(
                new InjectionConstructor(new AdminService()));
            container.RegisterType<IAuthenticationManager>(
    new InjectionFactory(c => HttpContext.Current.GetOwinContext().Authentication));
            container.RegisterType<IUserStore<ApplicationUser>,
            UserStore<ApplicationUser>>(new InjectionConstructor(new ApplicationDbContext()));

            container.RegisterType<IClientService, ClientService>();
           container.RegisterType<IAdminService, AdminService>();

            container.RegisterType<IAccountService, AccountService>();
            container.RegisterType<IRecordService, RecordService>();
            container.RegisterType<IClientService, ClientService>();
            container.RegisterType<IAdminService, AdminService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IAccountService, AccountService>();

        }
    }
}
