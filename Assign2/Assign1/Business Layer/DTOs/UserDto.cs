﻿namespace Assign1.Business_Layer.DTOs
{
    public class UserDto
    {
        public int IdUser { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
    }
}
