﻿namespace Assign1.Business_Layer.DTOs
{
    public class RecordDto
    {
        public int AccountSender { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public int AccountRecipient { get; set; }
        public string UserId { get; set; }
        public System.DateTime Date { get; set; }
        public int IdRecord { get; set; }
    }
}
