﻿namespace Assign1.Business_Layer.DTOs
{
    public class ClientDto
    {
        public string SSC { get; set; }
        public string Name { get; set; }
        public int IdentityCard { get; set; }
        public string Address { get; set; }

    }
}
