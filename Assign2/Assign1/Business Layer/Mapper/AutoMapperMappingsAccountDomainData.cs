﻿using Assign1.Business_Layer.DTOs;
using Assign1.Data_Layer.DataModels;
using AutoMapper;

namespace Assign1.Business_Layer.Mapper
{
    public class AutoMapperMappingsAccountDomainData : Profile
    {
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<BankAccountDto, Data_Layer.DataModels.BankAccount>()
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}
