﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assign1.Business_Layer.DTOs;
using AutoMapper;

namespace Assign1.Business_Layer.Mapper
{
    public class AutoMapperMappingsRecordToRecordDto : Profile
    {
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<Data_Layer.DataModels.Record, RecordDto>()
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}