﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Assign1.Business_Layer.DTOs;
using Assign1.Data_Layer.DataModels;
using AutoMapper;

namespace Assign1.Business_Layer.Mapper
{
    public class AutoMapperMappingsRecordDtoToRecord : Profile
    {
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<RecordDto, Record > ()
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}