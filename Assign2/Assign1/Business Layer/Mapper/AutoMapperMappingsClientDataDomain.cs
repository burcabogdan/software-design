﻿using Assign1.Business_Layer.DTOs;
using Assign1.Data_Layer.DataModels;
using AutoMapper;

namespace Assign1.Business_Layer.Mapper
{
    public class AutoMapperMappingsClientDataDomain : Profile
    {
        public override string ProfileName => "AutoMapperMappingsBusiness";

        protected override void Configure()
        {
            this.CreateMap<ClientDto, Data_Layer.DataModels.Client>()
                .WithProfile("AutoMapperMappingsBusiness");
        }
    }
}