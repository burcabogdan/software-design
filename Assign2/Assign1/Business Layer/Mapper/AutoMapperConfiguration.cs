﻿namespace Assign1.Business_Layer.Mapper
{
    public class AutoMapperConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(x =>
            {
              //  x.AddProfile<AutoMapperMappings>();
             //   x.AddProfile<AutoMapperMappingsDtoToData>();
                x.AddProfile<AutoMapperMappingsAccountDomainData>();
                x.AddProfile<AutoMapperMappingsAccountDataDomain>();
                x.AddProfile<AutoMapperMappingsClientDataDomain>();
                x.AddProfile<AutoMapperMappingsClientDomainData>();
                x.AddProfile<AutoMapperMappingsRecordDtoToRecord>();
                x.AddProfile<AutoMapperMappingsRecordToRecordDto>();
                //    x.AddProfile<AutoMapperMappingsDtoToData>();

            });
        }
    }
}