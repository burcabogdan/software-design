﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Data_Layer.DataModels;
using Assign1.Data_Layer.Interfaces;
using Microsoft.AspNet.Identity;

namespace Assign1.Business_Layer.Services
{
    public class AccountService : IAccountService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IRepository<BankAccount> _accountRepository;
        private readonly IRepository<Record> _recordRepository;
        private IRecordService RecordService { get; set; }

        public AccountService() { }
        public AccountService(IRecordService recordService)
        {
            _unitOfWork = new UnitOfWork();
            _accountRepository = _unitOfWork.AccountRepository;
            _recordRepository = _unitOfWork.RecordRepository;
            RecordService = recordService;
        }

        public void AddAccount(BankAccountDto Account)
        {
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccount>();
            var AccountData = AutoMapper.Mapper.Map<BankAccountDto, BankAccount>(Account);
            if(_accountRepository.GetById(Account.IdAccount) != null && _accountRepository.GetById(Account.IdAccount).IdAccount == Account.IdAccount)
                throw new PrimaryKeyException("Account data already exists. Please try again.");
            _accountRepository.Insert(AccountData);
            _unitOfWork.Save();
        }

        public void AddAccount(string name, int id, string password, bool isAdmin)
        {
            //DataLayer.Account currentAccountState = new DataLayer.Account
            //{
                
              
            //    name = name,
            //    idAccount = id,
            //    isAdmin = (byte)(isAdmin == true ? 1 : 0),
            //    password = password
            //};
            //_accountRepository.Insert(currentAccountState);
            //_unitOfWork.Save();
        }

        public BankAccountDto GetAccountById(int id)
        {
            BankAccount account = _accountRepository.GetById(id);
            AutoMapper.Mapper.CreateMap<BankAccount, BankAccountDto>();
            var AccountDtos = AutoMapper.Mapper.Map<BankAccount, BankAccountDto>(account);
            return AccountDtos;
        }

        
        public void UpdateAccount(BankAccountDto accountDto)
        {
            var account = _accountRepository.GetById(accountDto.IdAccount);
            //account.sscClient = accountDto.sscClient;
            account.Balance = accountDto.Balance;
            account.Interest = accountDto.Interest;
            account.Type = accountDto.Type;
            account.Fee = accountDto.Fee;
            _accountRepository.Update(account);
            _unitOfWork.Save();
        }

        public List<BankAccountDto> GetAllAccounts()
        {
            var Accounts = _accountRepository.Get();
            AutoMapper.Mapper.CreateMap<BankAccount, BankAccountDto>();
            var AccountDtos = AutoMapper.Mapper.Map<List<BankAccount>, List<BankAccountDto>>((List<BankAccount>)Accounts);
            return AccountDtos;
        }

        public List<BankAccountDto> GetAllClientAccounts(string id)
        {
            var Accounts = _accountRepository.Get().Where(x => x.sscClient.Equals(id));
            AutoMapper.Mapper.CreateMap<Data_Layer.DataModels.BankAccount, BankAccountDto>();
            var AccountDtos = AutoMapper.Mapper.Map<List<Data_Layer.DataModels.BankAccount>, List<BankAccountDto>>(Accounts.ToList());
            return AccountDtos;
        }

        public void PerformTranscation(int sourceId, int destinationId, int sum, string type, string id)
        {
            var sourceAccount = _accountRepository.GetById(sourceId);
            var destinationAccount = _accountRepository.GetById(destinationId);
            AutoMapper.Mapper.CreateMap<BankAccount, BankAccountDto>();
            var sourceAccountDto = AutoMapper.Mapper.Map<BankAccount, BankAccountDto>(sourceAccount);
            var destinationAccountDto = AutoMapper.Mapper.Map<BankAccount, BankAccountDto>(destinationAccount);

            if (sourceAccountDto.Balance - sum < 0) return;
            destinationAccountDto.Balance += sum;
            sourceAccountDto.Balance -= sum;
            UpdateAccount(destinationAccountDto);
            UpdateAccount(sourceAccountDto);
            RecordDto record = new RecordDto();
            record.AccountSender = sourceId;
            record.AccountRecipient = destinationId;
            record.Amount = sum;
            record.IdRecord = RecordService.GetNewRecordId();
            record.Date = DateTime.Now;
            record.Type = type;
            record.UserId = id;
            RecordService.AddReport(record);
        }

        public void RemoveAccount(int id)
        {
            _accountRepository.Delete(id);
            _unitOfWork.Save();
        }
    }
}
