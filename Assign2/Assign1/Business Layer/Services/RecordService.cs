﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Data_Layer.DataModels;
using Assign1.Data_Layer.Interfaces;
using Assign1.Models;

namespace Assign1.Business_Layer.Services
{
    public class RecordService : IRecordService
    {
        #region Members
        private readonly UnitOfWork _unitOfWork;
        private readonly IRepository<Record> _recordRepository;
        private readonly IAdminService _adminService;
        #endregion

        public RecordService(IAdminService adminService)
        {
            _unitOfWork = new UnitOfWork();
            _recordRepository = new UnitOfWork().RecordRepository;
            _adminService = adminService;
        }

        public void AddReport(RecordDto recordDto)
        {
            AutoMapper.Mapper.CreateMap<RecordDto, Record>();
            var clientData = AutoMapper.Mapper.Map<RecordDto, Record>(recordDto);
            _recordRepository.Insert(clientData);
            _unitOfWork.Context.Entry(clientData).State = EntityState.Modified;
            _unitOfWork.Save();
        }

        public List<RecordDto> GetReportsByUser(string userId, DateTime start, DateTime end)
        {
            var records = _recordRepository.Get();
            records = records.Where(x => x.UserId.Equals(userId)).ToList();
            AutoMapper.Mapper.CreateMap<Record, RecordDto>();
            var recordsDtos = AutoMapper.Mapper.Map<List<Data_Layer.DataModels.Record>, List<RecordDto>>(records.ToList());
            return recordsDtos;
        }

        public List<RecordDto> GetReports()
        {
            var records = _recordRepository.Get();
            AutoMapper.Mapper.CreateMap<Record, RecordDto>();
            var recordsDtos = AutoMapper.Mapper.Map<List<Data_Layer.DataModels.Record>, List<RecordDto>>(records.ToList());
            return recordsDtos;
        }

        public int GetNewRecordId()
        {
            return _recordRepository.Get().OrderByDescending(x => x.IdRecord).Select(y => y.IdRecord).FirstOrDefault();
        }

        public List<RecordDto> SearchRecords(DateTime? StartDateTime, DateTime? EndDateTime, string UserName)
        {
            if(/*StartDateTime == null || EndDateTime == null || */StartDateTime > EndDateTime || String.IsNullOrEmpty(UserName))
                return new List<RecordDto>();


            var userId = _adminService.GetAllUsers().Where(p => p.UserName == UserName).Select(q => q.Id).FirstOrDefault();
            var records = _recordRepository.Get().Where(x => x.UserId == userId);
            AutoMapper.Mapper.CreateMap<Record, RecordDto>();
            var recordsDtos = AutoMapper.Mapper.Map<List<Data_Layer.DataModels.Record>, List<RecordDto>>(records.ToList());
            var result = recordsDtos;
            if (StartDateTime == null || EndDateTime == null)
                return result;
            return result.Where(y => DateTime.Compare(y.Date, (DateTime)StartDateTime) > 0 && DateTime.Compare(y.Date, (DateTime)EndDateTime) < 0).ToList();
        }
    }
}
