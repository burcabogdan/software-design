﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Assign1.Business_Layer.DTOs;
using Assign1.Business_Layer.Interfaces;
using Assign1.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace Assign1.Controllers
{
    [Authorize]
    public class BankAccountController : Controller
    {
        private readonly IAccountService _accountService;

        public BankAccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        public ActionResult Index()
        {
            var bankAccounts = _accountService.GetAllAccounts();
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccountViewModel>();
            var bankAccountsViewModels = AutoMapper.Mapper.Map<List<BankAccountDto>, List<BankAccountViewModel>>(bankAccounts);
            return View(bankAccountsViewModels);
        }

        [HttpGet]
        public ActionResult AddBankAccount(string id)
        {
            BankAccountViewModel b = new BankAccountViewModel();
            b.sscClient = id;
            return View(b);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddBankAccount(BankAccountViewModel Client)
        {
            try
            {
                Client.DateOfCreation = DateTime.Now;
                AutoMapper.Mapper.CreateMap<BankAccountViewModel, BankAccountDto>();
                var ClientDto = AutoMapper.Mapper.Map<BankAccountViewModel, BankAccountDto>(Client);
                _accountService.AddAccount(ClientDto);


                return View("GetBankAccounts", _accountService.GetAllClientAccounts(Client.sscClient));

            }
            catch (PrimaryKeyException e)
            {
                return View(Client);
            }
        }

        [HttpGet]
        public ActionResult EditBankAccount(int id)
        {
            var Client = _accountService.GetAccountById(id);
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccountViewModel>();
            var ClientViewModel = AutoMapper.Mapper.Map<BankAccountDto, BankAccountViewModel>(Client);

            return View("EditBankAccount", ClientViewModel);
        }

        [HttpPost]
        public ActionResult EditBankAccount(BankAccountViewModel b)
        {
            AutoMapper.Mapper.CreateMap<BankAccountViewModel, BankAccountDto>();
            var account = AutoMapper.Mapper.Map<BankAccountViewModel, BankAccountDto>(b);

            _accountService.UpdateAccount(account);
            var Client = _accountService.GetAccountById(account.IdAccount);
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccountViewModel>();
            var ClientViewModel = AutoMapper.Mapper.Map<BankAccountDto, BankAccountViewModel>(Client);
            return View("EditBankAccount", ClientViewModel);
        }

        public ActionResult DeleteBankAccount(int id)
        {
            string ssc = _accountService.GetAccountById(id).sscClient;
            _accountService.RemoveAccount(id);
            var clients = _accountService.GetAllClientAccounts(ssc);
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccountViewModel>();
            var clientViewModels = AutoMapper.Mapper.Map<List<BankAccountDto>, List<BankAccountViewModel>>(clients);
            return View("Index", clientViewModels);
        }

        [HttpGet]
        public ActionResult TransferMoney(int id)
        {
            var Client = _accountService.GetAccountById(id);
            AutoMapper.Mapper.CreateMap<BankAccountDto, BankAccountViewModel>();
            var ClientViewModel = AutoMapper.Mapper.Map<BankAccountDto, BankAccountViewModel>(Client);

            var transfer = new TransferViewModel();
            transfer.AccountSender = ClientViewModel.IdAccount;
            transfer.AccountsList = _accountService.GetAllAccounts().Select(x => x.IdAccount).ToList();
            return View("TransferMoney", transfer);
        }

        [HttpPost]
        public ActionResult TransferMoney(TransferViewModel transfer)
        {
            ApplicationUser user = System.Web.HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>().FindById(System.Web.HttpContext.Current.User.Identity.GetUserId());

            _accountService.PerformTranscation(transfer.AccountSender, transfer.AccountRecipient, transfer.Amount, transfer.Type, user.Id);

            return RedirectToAction("GetAllClientAccounts", "Client", new {id = _accountService.GetAccountById(transfer.AccountSender).sscClient});
        }

    }
}
