﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using Assign1.Data_Layer.DataModels;

namespace Assign1.Models
{
    public class ClientViewModel 
    {
        [Required(ErrorMessage = "{0} is a required field.")]
        [DataType(DataType.Text)]
        [Display(Name = "Social Security Number")]
        [RegularExpression(@"^(\d{3}-?\d{2}-?\d{4}|XXX-XX-XXXX)$", ErrorMessage = "{0} is required and must be of XXX-XX-XXXX format.")]
        [Index("SSC", IsUnique = true)]
        public string SSC { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Name")]
        [RegularExpression(@"^[A-Za-z ]+$", ErrorMessage = "No numbers are allowed in the {0}.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "{0} is a required field.")]
        [DataType(DataType.Text)]
        [Display(Name = "Identity Card Number")]
        [RegularExpression(@"^[0-9]{1,6}$", ErrorMessage = "{0} is required and must be of XXX-XX-XXXX format.")]
        public int IdentityCard { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Address")]
        public string Address { get; set; }
    }
}