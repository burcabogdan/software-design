﻿namespace Assign1.Models
{
    public class RecordViewModel
    {
        public int AccountSender { get; set; }
        public string Type { get; set; }
        public decimal Amount { get; set; }
        public int AccountRecipient { get; set; }
        public string UserId { get; set; }
        public System.DateTime Date { get; set; }
        public int IdRecord { get; set; }
    }
}