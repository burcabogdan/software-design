﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Assign1.Data_Layer;
using Assign1.Data_Layer.DataModels;
using Assign1.Migrations;
using Assign1.Models;
using FluentValidation.Attributes;
using FluentValidation.Mvc;

namespace Assign1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
           // BankInitializer bankInitializer = new BankInitializer();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());

          //  FluentValidationModelValidatorProvider.Configure();

        }
    }
}
