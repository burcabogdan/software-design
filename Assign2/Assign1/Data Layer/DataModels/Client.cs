﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Assign1.Data_Layer.DataModels
{
    public sealed class Client
    {
        [DataMember, Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SSC { get; set; }
        public string Name { get; set; }
        public int IdentityCard { get; set; }
        public string Address { get; set; }

        public ICollection<BankAccount> Accounts { get; set; }
    }
}