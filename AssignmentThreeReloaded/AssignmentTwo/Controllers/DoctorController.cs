﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using AssignmentTwo.Models;
using AssignmentTwo.Services;

namespace AssignmentTwo.Controllers
{
    public class DoctorController : Controller
    {
        private IConsultationService _consultationService;
        private IPatientService _patientService;

        public DoctorController()
        {
            _consultationService = new ConsultationService();
            _patientService = new PatientService();
        }

        // GET: Doctor
        [HttpGet]
        public ActionResult Index()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Doctor") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");

            var res = _consultationService.GetConsulations();
            return View(res);
        }

        [HttpPost]
        public ActionResult Index(string patient)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Doctor") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");


            var x = Regex.Match(patient, @"\d+").Value;
            var res =
                _consultationService.GetConsultationByPatientId(x).Where(e => String.IsNullOrEmpty(e.Diagnostic)); ;
                    
            return View(res);
        }

        [HttpGet]
        public ActionResult EditConsultation(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Doctor") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");

            var Consultation = _consultationService.GetConsultationById(id);
            //if (user.Role.)
            //    return RedirectToAction("GetNormalConsultations");
            return View(Consultation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditConsultation(Consultation user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Doctor") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");

            try
            {
                _consultationService.EditConsultation(user);

                return RedirectToAction("Index");
            }
            catch (PrimaryKeyException e)
            {
                return RedirectToAction("Index");
            }
        }

        public JsonResult CheckIfCorrectDoctor(string information)
        {
            var currentUser = Session["user"] as UserModel;
            var doctor = Regex.Match(information, @"\d+").Value;
            bool x = true;
            return Json(new { Result = x });

        }
    }
}