﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using AssignmentTwo.Models;
using AssignmentTwo.Services;
using Microsoft.Ajax.Utilities;

namespace AssignmentTwo.Controllers
{
    public class ConsultationController : Controller
    {
        private IConsultationService _consultationService;
        private IPatientService _patientService;

        public ConsultationController()
        {
            _consultationService = new ConsultationService();
            _patientService = new PatientService();
        }


        public ActionResult GetConsultation()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var result = _consultationService.GetConsulations();
            return View(result);
        }

        [HttpGet]
        public ActionResult AddConsultation()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            return View(new Consultation
            {
                StartDateTime = DateTime.Now,
                EndDateTime = DateTime.Now,
                ConsulationId = _consultationService.GetNextId()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddConsultation(Consultation user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                if(!_consultationService.ValidateConsultation(user.DoctorID, user.StartDateTime, user.EndDateTime))
                    return View("ScheduleConsultation");

                _consultationService.InsertConsultation(user);

                return RedirectToAction("GetConsultation");
            }
            catch (PrimaryKeyException e)
            {
                return View("PrimaryKeyError");
            }
        }


        [HttpGet]
        public ActionResult EditConsultation(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var Consultation = _consultationService.GetConsultationById(id);
            //if (user.Role.)
            //    return RedirectToAction("GetNormalConsultations");
            return View(Consultation);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditConsultation(Consultation user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                if (!_consultationService.ValidateConsultation(user.DoctorID, user.StartDateTime, user.EndDateTime))
                    return View("ScheduleConsultation");
                _consultationService.EditConsultation(user);

                return RedirectToAction("GetConsultation");
            }
            catch (PrimaryKeyException e)
            {
                return View("PrimaryKeyError");
            }
        }

        [HttpGet]
        public ActionResult DeleteConsultation(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            _consultationService.DeleteConsultation(id);
            return RedirectToAction("GetConsultation");
        }

        [HttpGet]
        public ActionResult CheckInPatient()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var xs = _consultationService.GetConsulations();
            List<Patient> p = new List<Patient>();
            List<String> strings = new List<string>();
            foreach (var ds in xs)
            {
                if (_patientService.GetPatientByPNC(ds.PatientPNC) != null)
                {
                    var temp = ds.ConsulationId + " " +  ds.DoctorID + " " + ds.PatientPNC + " " + _patientService.GetPatientByPNC(ds.PatientPNC).Name;
                    strings.Add(temp);
                }
            }
            var patients = p.DistinctBy(xss=> xss.PNC).ToList();
            var x = _patientService.PPatientsToTring(patients);
            var model = new PatientCheckIn
            {
                Patients = strings
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult CheckInPatient(PatientCheckIn model)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            ViewBag.User =  Session["user"] as UserModel;
            return View(model);
        }

        public ActionResult Chat()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            return View();
        }

    }
}