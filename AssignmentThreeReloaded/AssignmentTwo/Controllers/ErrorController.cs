﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AssignmentTwo.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult PrimaryKeyError(string id)
        {
            return View(id);
        }
    }
}