﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssignmentTwo.Models;
using AssignmentTwo.Services;

namespace AssignmentTwo.Controllers
{
    public class PatientController : Controller
    {
        private IPatientService PatientService;

        public PatientController()
        {
            PatientService = new PatientService();
        }


        public ActionResult GetPatient()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var result = PatientService.GetPatients();
            return View(result);
        }

        [HttpGet]
        public ActionResult AddPatient()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddPatient(Patient user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                PatientService.InsertPatient(user);

                return RedirectToAction("GetPatient");
            }
            catch (PrimaryKeyException e)
            {
                return View("PrimaryKeyErrorUser");
            }
        }


        [HttpGet]
        public ActionResult EditPatient(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var patient = PatientService.GetPatientByPNC(id);
            //if (user.Role.)
            //    return RedirectToAction("GetNormalPatients");
            return View(patient);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPatient(Patient user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                PatientService.EditPatient(user);

                return RedirectToAction("GetPatient");
            }
            catch (PrimaryKeyException e)
            {
                return View("PrimaryKeyErrorUser");
            }
        }

        [HttpGet]
        public ActionResult DeletePatient(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            PatientService.DeletePatient(id);
            return RedirectToAction("GetPatient");
        }

    }
}