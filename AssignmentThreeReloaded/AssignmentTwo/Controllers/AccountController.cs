﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.SessionState;
using System.Xml.Linq;
using AssignmentTwo.Models;
using AssignmentTwo.Services;
using SemesterosProjectos.BusinessLayer;

namespace AssignmentTwo.Controllers
{
    public class AccountController : Controller
    {
        public IUserService _userRepository;

        public AccountController()
        {
            _userRepository = new UserService();
        }
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Username, string Password)
        {
            var user = _userRepository.GetUserByUserName(Username);
            user.IsLoggedIn = true;
            _userRepository.EditUserModel(user);
            if (user != null && user.Password.Equals(Password))
            {
                HitCounter();
                Session["user"] = user;
                return RedirectToAction("Index", "Home");
            }
            return Login();
        }

        public ActionResult Logout()
        {
            var currentUser = Session["user"] as UserModel;
            var user = _userRepository.GetUserByUserName(currentUser.UserName);
            user.IsLoggedIn = false;
            _userRepository.EditUserModel(user);

            Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult GetNormalEmployees()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var result = _userRepository.GetUsers();
            return View(result);
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddEmployee(UserModel user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                _userRepository.InsertUserModel(user);

                return RedirectToAction("GetNormalEmployees");
            }
            catch (PrimaryKeyException e)
            {
                return View("PrimaryKeyErrorUser");
            }
        }


        [HttpGet]
        public ActionResult EditEmployee(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var user = _userRepository.GetUserByUserName(id);
            if (user.Role.Equals("Administrator"))
                return RedirectToAction("GetNormalEmployees");
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditEmployee(UserModel user)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                _userRepository.EditUserModel(user);

                return RedirectToAction("GetNormalEmployees");
            }
            catch (PrimaryKeyException e)
            {
                return View("PrimaryKeyErrorUser");
            }
        }

        [HttpGet]
        public ActionResult DeleteEmployee(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            if (_userRepository.GetUserByUserName(id).Role.Equals("Administrator"))
                return RedirectToAction("GetNormalEmployees");
            _userRepository.DeleteUserModel(id);
            return RedirectToAction("GetNormalEmployees");
        }

        public JsonResult GetCurrentUser()
        {
            var currentUser = Session["user"] as UserModel;
            return Json(new { Result = currentUser});
        }

        public ActionResult GetCurrentUserCount()
        {
            var count = _userRepository.GetUsers().Count(x => x.IsLoggedIn);
            return Json(new { Result = count }, JsonRequestBehavior.AllowGet);
        }

        public void HitCounter()
        {
            string fullPath = HttpContext.Server.MapPath("~/counterXML.xml");
            XDocument xDoc = XDocument.Load(fullPath);
            int hits = int.Parse(xDoc.Descendants("counter").First().Value.Trim()) + 1;
            xDoc.Descendants("counter").First().SetValue(hits);
            xDoc.Save(fullPath);
        }

        public string ReadCounter()
        {
            string fullPath = HttpContext.Server.MapPath("~/counterXML.xml");
            XDocument xDoc = XDocument.Load(fullPath);
            int hits = int.Parse(xDoc.Descendants("counter").First().Value.Trim());
            return hits.ToString();
        }

        public ActionResult GenerateXMLReport()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");

            Response.ClearContent();
            Response.Output.Write("Report." + "xml"); //+ Environment.NewLine);

            Response.ClearHeaders();
            Response.ContentType = "text/xml";
            Response.AddHeader("Content-Disposition", "attachment; filename=Report." + "xml");

                XDocument doc = new XDocument(new XElement("body",
                       new XElement("Information",
                           new XElement("HitCount", ReadCounter()),
                           new XElement("NumberOfUsers", _userRepository.GetUsers().Count)
                           ),
                       new XElement("ShareCounts")));
            var xElement = doc.Elements("ShareCounts").DescendantNodes().ToList();
                ArticleService articleService = new ArticleService();
                var articles = articleService.GetArticles();
                foreach (var article in articles)
                {
                    xElement.Add(new XElement(article.Title + "Id" + article.ArticleId, article.ShareCount));
                }
                doc.Save(Response.Output);
           Response.Write(doc.ToString());

            Response.End();
            Response.Flush();
            Response.Clear();

            return RedirectToAction("GetNormalEmployees", "Account");
        }

    }
}