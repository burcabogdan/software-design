﻿using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;
using System.Web.Routing;
using AssignmentTwo;
using AssignmentTwo.BusinessLayer;
using AssignmentTwo.Models;
using AssignmentTwo.Services;
using SemesterosProjectos.BusinessLayer;
using SemesterosProjectos.Models;

namespace SemesterosProjectos.Controller
{
    public class ArticleController : System.Web.Mvc.Controller
    {
        public IArticleService ArticleService;
        public ICommentService CommentService;
        public IUserService UserService;
        public ArticleController()
        {
            ArticleService = new ArticleService();
            CommentService = new CommentService();
            UserService = new UserService();
        }

        [HttpGet]
        public ActionResult Index()
        {
            var articles = ArticleService.GetArticles();
            return View(articles);
        }

        [HttpPost]
        public ActionResult Index(string author)
        {
            var articles = ArticleService.GetArticlesByAuthor(author).ToList();
            return View(articles);
        }

        
        public ActionResult GetFlaggedArticles()
        {
            var articles = ArticleService.GetArticles().Where(x => x.IsFlagged).ToList();
            return View("Index", articles);
        }

        public ActionResult GetArticle(int id)
        {
            var article = ArticleService.GetArticleById(id);
            var comments = CommentService.GetCommentsByArticle(id);
            //var comments = D
            ViewBag.ArticleId = article.ArticleId;
            TempData["FeaturedProduct"] = article.ArticleId;

            var articlePage = new ArticlePageViewModel
            {
                Article = article,
                Comments = comments
            };
            return View(articlePage);
        }

        public ActionResult Delete(int id)
        {
            ArticleService.DeleteArticle(id);
            //var comments = D
            return RedirectToAction("Index", "Article");
        }

        [HttpGet]
        public ActionResult Create()
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Writer") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Article article)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || currentUser != null && !(currentUser.Role.Equals("Secretary") || currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {
                if (ArticleService.GetArticleById(article.ArticleId) != null)
                    return View("ErrorArticleKey");

                ArticleService.InsertArticle(article);

                return RedirectToAction("Index");
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult PostComment(ArticlePageViewModel article)
        {
            var articleId = TempData["FeaturedProduct"];

            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");
            try
            {
                Comment comment = new Comment();
                int resultId;
                Int32.TryParse(articleId.ToString(), out resultId);
                comment.ArticleId = resultId;
                comment.CommenterId = currentUser.UserName;
                int nextComId = CommentService.GetComments().Select(x => x.CommentId).OrderByDescending(t => t).FirstOrDefault();

                comment.CommentId = nextComId++;
                comment.IsFlagged = false;
                comment.Content = article.NewComment.Content;
                CommentService.InsertComment(comment);
                //ArticleService.InsertArticle(article);

                var carticle = ArticleService.GetArticleById(resultId);
                var comments = CommentService.GetCommentsByArticle(resultId);

                var writerEmail = UserService.GetUserByUserName(carticle.Writer).Email;
                var fromAddress = new MailAddress("burca123456@gmail.com", "From Name");
                var toAddress = new MailAddress(writerEmail, "To Name");
                const string fromPassword = "testing12";
                const string subject = "Subject";
                string body = currentUser.UserName + "commented on article with id " + carticle.ArticleId + ", named " + carticle.Title;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }








                var articlePage = new ArticlePageViewModel
                {
                    Article = carticle,
                    Comments = comments
                };
               // return View("GetArticle", articlePage);
                return RedirectToAction("GetArticle", new { id = carticle.ArticleId });

                //return RedirectToAction("Index");
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }
        [HttpPost]
        public ActionResult PostCommenta(string id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");
            try
            {
                //if (ArticleService.GetArticleById(article.ArticleId) != null)
                //    return View("ErrorArticleKey");

                //ArticleService.InsertArticle(article);

                return RedirectToAction("Index");
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }

        [HttpPost]
        public ActionResult FlagArticle(ArticlePageViewModel article)
        {
            var articleId = TempData["FeaturedProduct"];

            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");
            try
            {
                Comment comment = new Comment();
                int resultId;
                Int32.TryParse(articleId.ToString(), out resultId);

                var carticle = ArticleService.GetArticleById(resultId);
                carticle.IsFlagged = true;
                ArticleService.EditArticle(carticle);
                var comments = CommentService.GetCommentsByArticle(resultId);

                var articlePage = new ArticlePageViewModel
                {
                    Article = carticle,
                    Comments = comments
                };
                return RedirectToAction("GetArticle", new { id = carticle.ArticleId });
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            var user = ArticleService.GetArticleById(id);
            return View(user);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Article article)
        {
            var currentUser = Session["user"] as UserModel;
            if (currentUser == null || (currentUser != null && !currentUser.Role.Equals("Administrator")))
                return RedirectToAction("Index", "Home");
            try
            {

                ArticleService.EditArticle(article);

                return RedirectToAction("Index");
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }


        [HttpPost]
        public ActionResult ShareArticle(ArticlePageViewModel article)
        {
            var articleId = TempData["FeaturedProduct"];

            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");
            try
            {
                int resultId;
                Int32.TryParse(articleId.ToString(), out resultId);

                var carticle = ArticleService.GetArticleById(resultId);
                var comments = CommentService.GetCommentsByArticle(resultId);

                carticle.ShareCount++;
                ArticleService.EditArticle(carticle);
                foreach (var writerEmail in UserService.GetUsers().Select(x => x.Email))
                {
                    var fromAddress = new MailAddress("burca123456@gmail.com", "MMA Blog");
                    var toAddress = new MailAddress(writerEmail, "To Name");
                    const string fromPassword = "testing12";
                    const string subject = "Subject";
                    string body = currentUser.UserName + " shared the article named " + carticle.Title
                        + ". Link http://localhost:12271/article/GetArticle/" + carticle.ArticleId;

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        Body = body
                    })
                    {
                        smtp.Send(message);
                    }
                }

                var articlePage = new ArticlePageViewModel
                {
                    Article = carticle,
                    Comments = comments
                };
                return RedirectToAction("GetArticle", new { id = carticle.ArticleId });
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }

        public ActionResult ReplyToComment(string id)
        {
            string number = new String(id.TakeWhile(Char.IsDigit).ToArray());
            int idd;
            string  content = new String(id.Where(Char.IsLetter).ToArray());

            var articleId = TempData["FeaturedProduct"];
            Int32.TryParse(number, out idd);

            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");
            try
            {
                Comment comment = new Comment();
                int resultId;
                Int32.TryParse(articleId.ToString(), out resultId);
                comment.ArticleId = resultId;
                comment.CommenterId = currentUser.UserName;
                int nextComId = CommentService.GetComments().Select(x => x.CommentId).OrderByDescending(t => t).FirstOrDefault();
                //var article = ArticleService.GetArticleById(articleId);
                comment.CommentId = nextComId++;
                comment.IsFlagged = false;
                comment.Content = "Reply to comment with id " + idd + content;//article.NewComment.Content;
                CommentService.InsertComment(comment);

                var carticle = ArticleService.GetArticleById(resultId);
                var comments = CommentService.GetCommentsByArticle(resultId);

                var writerEmail = UserService.GetUserByUserName(carticle.Writer).Email;
                var fromAddress = new MailAddress("burca123456@gmail.com", "From Name");
                var toAddress = new MailAddress(writerEmail, "To Name");
                const string fromPassword = "testing12";
                const string subject = "Subject";
                string body = currentUser.UserName + "commented on article with id " + carticle.ArticleId + ", named " + carticle.Title;

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }


                var articlePage = new ArticlePageViewModel
                {
                    Article = carticle,
                    Comments = comments
                };
                return RedirectToAction("GetArticle", new { id = carticle.ArticleId });

            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }

        public ActionResult DeleteComment(int id)
        {
            var articleId = TempData["FeaturedProduct"];

            var currentUser = Session["user"] as UserModel;
            if (currentUser == null)
                return RedirectToAction("Index", "Home");
            try
            {
                Comment comment = new Comment();
                int resultId;
                Int32.TryParse(articleId.ToString(), out resultId);
                comment.ArticleId = resultId;
                comment.CommenterId = currentUser.UserName;
                int nextComId = CommentService.GetComments().Select(x => x.CommentId).OrderByDescending(t => t).FirstOrDefault();
                //var article = ArticleService.GetArticleById(articleId);
                comment.CommentId = nextComId++;
                comment.IsFlagged = false;
                comment.Content = "Reply to comment with id " + id;// + content;//article.NewComment.Content;
                CommentService.InsertComment(comment);
                //ArticleService.InsertArticle(article);

                var carticle = ArticleService.GetArticleById(resultId);
                var comments = CommentService.GetCommentsByArticle(resultId);
                CommentService.DeleteComment(id);

                
                //var writerEmail = UserService.GetUserByUserName(carticle.Writer).Email;
                //var fromAddress = new MailAddress("burca123456@gmail.com", "From Name");
                //var toAddress = new MailAddress(writerEmail, "To Name");
                //const string fromPassword = "testing12";
                //const string subject = "Subject";
                //string body = currentUser.UserName + "commented on article with id " + carticle.ArticleId + ", named " + carticle.Title;

                //var smtp = new SmtpClient
                //{
                //    Host = "smtp.gmail.com",
                //    Port = 587,
                //    EnableSsl = true,
                //    DeliveryMethod = SmtpDeliveryMethod.Network,
                //    UseDefaultCredentials = false,
                //    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                //};
                //using (var message = new MailMessage(fromAddress, toAddress)
                //{
                //    Subject = subject,
                //    Body = body
                //})
                //{
                //    smtp.Send(message);
                //}








                var articlePage = new ArticlePageViewModel
                {
                    Article = carticle,
                    Comments = comments
                };
                // return View("GetArticle", articlePage);
                return RedirectToAction("GetArticle", new { id = carticle.ArticleId });

                //return RedirectToAction("Index");
            }
            catch (PrimaryKeyException e)
            {
                return View("Error");
            }
        }

    }
}