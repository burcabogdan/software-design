﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AssignmentTwo.Startup))]
namespace AssignmentTwo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
