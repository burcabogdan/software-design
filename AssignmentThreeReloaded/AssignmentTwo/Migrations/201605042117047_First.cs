namespace AssignmentTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class First : DbMigration
    {
        public override void Up()
        {

            CreateTable(
                "dbo.Consultation",
                c => new
                    {
                        ConsulationId = c.Int(nullable: false),
                        DoctorID = c.String(),
                        PatientPNC = c.String(),
                        StartDateTime = c.DateTime(nullable: false),
                        EndDateTime = c.DateTime(nullable: false),
                        Diagnostic = c.String()
                })
                .PrimaryKey(t => t.ConsulationId);

            CreateTable(
                "dbo.Patient",
                c => new
                    {
                        PNC = c.String(nullable: false, maxLength: 128),
                        Name = c.String(),
                        IdentityCardNumber = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.PNC);
            
            CreateTable(
                "dbo.UserModel",
                c => new
                    {
                        UserName = c.String(nullable: false, maxLength: 100),
                        Name = c.String(nullable: false, maxLength: 100),
                        Email = c.String(nullable: false),
                        Password = c.String(maxLength: 100),
                        Role = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.UserName);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.UserModel");
            DropTable("dbo.Patient");
            DropTable("dbo.Consultation");
        }
    }
}
