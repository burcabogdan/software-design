using System.Collections.Generic;
using AssignmentTwo.Models;
using SemesterosProjectos.Models;

namespace AssignmentTwo.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MyDbContext context)
        {
            //var patients = new List<Patient>
            //{
            //    new Patient {PNC = "1234516789", IdentityCardNumber = "123459", DateOfBirth = new DateTime(2015, 12, 5), Name = "Burca BogdanPatient", Address = "Gheorghe Dima"},
            //    new Patient {PNC = "1234526788", IdentityCardNumber = "123458", DateOfBirth = new DateTime(2015, 12, 6), Name = "Burca BogdanPatientDoi", Address = "Gheorghe Dima"},
            //    new Patient {PNC = "1234536787", IdentityCardNumber = "123457", DateOfBirth = new DateTime(2015, 12, 7), Name = "Burca BogdanPatientTrei", Address = "Gheorghe Dima"},
            //};
            //patients.ForEach(s => context.Patients.AddOrUpdate(s));
            //context.SaveChanges();

            //var users = new List<UserModel>
            //{
            //    new UserModel
            //    {
            //        Email = "burcabogdan2007@yahoo.com",
            //        Name = "Burca Bogdan",
            //        Password = "password",
            //        Role = "Administrator",
            //        UserName = "burcabogdan"
            //    },
            //    new UserModel
            //    {
            //        Email = "burcabogdan20082@yahoo.com",
            //        Name = "Burca Doctor",
            //        Password = "password",
            //        Role = "Doctor",
            //        UserName = "burcadoctor"
            //    },
            //    new UserModel
            //    {
            //        Email = "burcabogdan22007@yahoo.com",
            //        Name = "Burca Secretary",
            //        Password = "password",
            //        Role = "Secretary",
            //        UserName = "burcasecretary"
            //    },
            //    new UserModel
            //    {
            //        Email = "burcabogdaan2008@yahoo.com",
            //        Name = "Burca Doctorski",
            //        Password = "password",
            //        Role = "Doctor",
            //        UserName = "burcadoctorski"
            //    }
            //};
            //users.ForEach(s => context.UserModels.AddOrUpdate(s));
            //context.SaveChanges();

            //var consultations = new List<Consultation>
            //{
            //    new Consultation
            //    {
            //        ConsulationId = "100",
            //        DoctorID = "burcadoctor",
            //        PatientPNC = "1234516789",
            //        StartDateTime = new DateTime(2016, 12, 5, 10, 30, 0),
            //        EndDateTime = new DateTime(2016, 12, 5, 10, 45, 0),
            //        Diagnostic = String.Empty
            //    },
            //    new Consultation
            //    {
            //        ConsulationId = "101",
            //        DoctorID = "burcadoctor",
            //        PatientPNC = "1234526788",
            //        StartDateTime = new DateTime(2016, 12, 5, 12, 30, 0),
            //        EndDateTime = new DateTime(2016, 12, 5, 12, 45, 0),
            //        Diagnostic = String.Empty
            //    },
            //    new Consultation
            //    {
            //        ConsulationId = "102",
            //        DoctorID = "burcadoctorski",
            //        PatientPNC = "1234536787",
            //        StartDateTime = new DateTime(2016, 12, 5, 14, 30, 0),
            //        EndDateTime = new DateTime(2016, 12, 5, 14, 45, 0),
            //        Diagnostic = String.Empty
            //    }
            //};
            //consultations.ForEach(s => context.Consultations.AddOrUpdate(s));
            //context.SaveChanges();

            var users = new List<UserModel>
            {
                new UserModel
                {
                    Email = "burcabogdan2007@yahoo.com",
                    Name = "Burca Bogdan",
                    Password = "password",
                    Role = "Administrator",
                    UserName = "burcabogdan"
                },
                new UserModel
                {
                    Email = "burca123456@gmail.com",
                    Name = "Burca Writer",
                    Password = "password",
                    Role = "Writer",
                    UserName = "burcawriter"
                },
                new UserModel
                {
                    Email = "burcabogdan93@gmail.com",
                    Name = "Burca Commenter",
                    Password = "password",
                    Role = "Regular",
                    UserName = "burcaregular"
                }
            };
            users.ForEach(s => context.UserModels.AddOrUpdate(s));
            context.SaveChanges();

            var articles = new List<Article>
            {
                new Article
                {
                    Title = "BoomTitle",
                    ArticleId = 101,
                    IsFlagged = false,
                    Writer = "burcawriter",
                    Content = @"What's going on with all the recent buzz about a possible fight between undefeated (and currently retired) boxing champion Floyd Mayweather taking on UFC featherweight champion Conor McGregor.
                    Just ask this guy.
                    It's a wild rumor, one apparently sparked by Money, who insists a boxing match against an MMA fighter is imminent, but one against Notorious would have to be approved by UFC President Dana White, who scoffed at the tabloid story (see his comments here).
                    Didn't keep the Vegas bookies from laying odds on the potential megafight.
                    Conor McGregor would be wasting Floyd's time, this longtime pugilist said about the fight. He can't beat Floyd in no boxing match, are you serious? He struggled with Nate Diaz and Nate Diaz is not a A-class boxer. You think Conor McGregor got a chance at boxing Floyd Mayweather?
                    Not after this.
                    That said, one thing I've learned about the combat sports business, which can bear expensive fruit when it wants to, is to never say never."
                }
            };
            articles.ForEach(s => context.Articles.AddOrUpdate(s));
            context.SaveChanges();

            var comments = new List<Comment>
            {
                new Comment
                {
                    Content = "Gotta love that fighting spirit.",
                    IsFlagged = false,
                    CommentId = 201,
                    CommenterId = "burcaregular",
                    ArticleId = 101
                }
            };
            comments.ForEach(s => context.Comments.AddOrUpdate(s));
            context.SaveChanges();
        }

    }
}

