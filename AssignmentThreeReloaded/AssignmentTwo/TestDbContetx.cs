﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using AssignmentTwo.Models;

namespace AssignmentTwo
{
    public class TestDbContetx : DbContext
    {
        public TestDbContetx() : base("DefaultConnection")
        {
        }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<UserModel> UserModels { get; set; }
        public DbSet<Consultation> Consultations { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}