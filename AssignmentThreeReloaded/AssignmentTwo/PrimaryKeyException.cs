﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentTwo
{
    public class PrimaryKeyException : Exception
    {
        public PrimaryKeyException() { }
        public PrimaryKeyException(string message) : base(message) { }
    }
}