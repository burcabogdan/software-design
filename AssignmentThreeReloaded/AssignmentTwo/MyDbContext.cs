﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using AssignmentTwo.Models;
using SemesterosProjectos.Models;

namespace AssignmentTwo
{

    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("DefaultConnection")
        {
        }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<UserModel> UserModels { get; set; }
        public DbSet<Consultation> Consultations { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Comment> Comments { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }

}