﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AssignmentTwo.Models
{
    public class PatientCheckIn
    {
        public List<String> Patients { get; set; }

        public string PNC { get; set; }

        public DateTime CheckInTime { get; set; }
    }
}