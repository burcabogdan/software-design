﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SemesterosProjectos.Models
{
    public class Comment
    {
        [Display(Name = "ArticleId")]
        [Required]
        public int ArticleId { get; set; }
        [Key]
        [Display(Name = "CommentId")]
        [Required]
        public int CommentId { get; set; }
        [Display(Name = "CommenterId")]
        [StringLength(50, ErrorMessage = "The content must be at least 6 characters long.")]
        [Required]
        public string CommenterId { get; set; }
        [Required]
        [Display(Name = "Content")]
        [StringLength(240, ErrorMessage = "The content must be at least 6 characters long.")]
        public string Content { get; set; }
        [Display(Name = "IsFlagged")]
        public bool IsFlagged { get; set; }
    }
}