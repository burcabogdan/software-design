﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssignmentTwo.Models
{
    public class Consultation
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Display(Name = "Consultation id")]
        [RegularExpression(@"^[0-9]{3}$", ErrorMessage = "3 digits are required in {0}.")]
        [Required]
        public string ConsulationId { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Doctor id")]
        [RegularExpression(@"^[0-9A-Za-z ]+$", ErrorMessage = "No characters other than letters and numbers are allowed in the {0}.")]
        public string DoctorID { get; set; }
        [Required(ErrorMessage = "{0} is a required field.")]
        [DataType(DataType.Text)]
        [Display(Name = "PNC")]
        [RegularExpression(@"^(97(8|9))?\d{9}(\d|X)$", ErrorMessage = "{0} is required and must be of 10.")]
        public string PatientPNC { get; set; }
        [DataType(DataType.Date)]
        public DateTime StartDateTime { get; set; }
        [DataType(DataType.Date)]
        public DateTime EndDateTime { get; set; }
        public String Diagnostic { get; set; }
    }
}