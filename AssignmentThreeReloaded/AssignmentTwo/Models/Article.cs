﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;

namespace SemesterosProjectos.Models
{
    public class Article
    {
        [Key]
        [Display(Name = "ArticleId")]
        [Required]
        [Index(IsUnique = true)]
        public int ArticleId { get; set; }
        [Required]
        [Display(Name = "Content")]
        [StringLength(55555, ErrorMessage = "The content must be at least 6 characters long.")]
        public string Content { get; set; }
        [Display(Name = "Writer")]
        [StringLength(50, ErrorMessage = "The content must be at least 6 characters long.")]
        [Required]
        [RegularExpression(@"^[A-Za-z ]+$", ErrorMessage = "No characters other than letters are allowed in the {0}.")]
        public string Writer { get; set; }
        public bool IsFlagged { get; set; }
        [Display(Name = "Title")]
        [StringLength(50, ErrorMessage = "The content must be at least 6 characters long.")]
        [Required]
        [RegularExpression(@"^[A-Za-z ]+$", ErrorMessage = "No characters other than letters are allowed in the {0}.")]
        public string Title { get; set; }
        public int ShareCount { get; set; }
    }
}