﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AssignmentTwo.Models
{
    public class Patient
    {
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Name")]
        [RegularExpression(@"^[A-Za-z ]+$", ErrorMessage = "No characters other than letters are allowed in the {0}.")]
        public string Name { get; set; }
        [Display(Name = "Identity Card")]
        [RegularExpression(@"^[0-9]{6}$", ErrorMessage = "6 digits are required in {0}.")]
        [Required]
        public string IdentityCardNumber { get; set; }
        [Key]
        [Required(ErrorMessage = "{0} is a required field.")]
        [DataType(DataType.Text)]
        [Display(Name = "PNC")]
        [RegularExpression(@"^(97(8|9))?\d{9}(\d|X)$", ErrorMessage = "{0} is required and must be of 10.")]
        [Index("PNC", IsUnique = true)]
        public string PNC { get; set; }
        [DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [Display(Name = "Address")]
        [RegularExpression(@"^[A-Za-z 0-9]+$", ErrorMessage = "No characters are allowed in the {0}.")]
        public string Address { get; set; }

    }
}