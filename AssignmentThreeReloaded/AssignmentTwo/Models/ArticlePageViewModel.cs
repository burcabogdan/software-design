﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SemesterosProjectos.Models;

namespace AssignmentTwo.Models
{
    public class ArticlePageViewModel
    {
        public ArticlePageViewModel()
        {
            Article = new Article();
            NewComment = new Comment();
            Comments = new List<Comment>();
            ReplyComment = new Comment();
        }

        public Article Article { get; set; }
        public List<Comment> Comments { get; set; }
        public Comment NewComment { get; set; }
        public Comment ReplyComment { get; set; }


    }
}