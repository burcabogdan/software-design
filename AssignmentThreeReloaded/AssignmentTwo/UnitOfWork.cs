﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using AssignmentTwo.Models;
using SemesterosProjectos.Models;

namespace AssignmentTwo
{
    public class UnitOfWork : IDisposable
    {
        //here
        private readonly MyDbContext _context = new MyDbContext();
        private GenericRepository<Patient> _patientRepository;
        private GenericRepository<Consultation> _consultationRepository;
        private GenericRepository<UserModel> _userRepository;
        private GenericRepository<Article> _articleRepository;
        private GenericRepository<Comment> _commentRepository;


        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (System.Data.Entity.Validation.DbEntityValidationException dbValEx)
            {
                var outputLines = new StringBuilder();
                foreach (var eve in dbValEx.EntityValidationErrors)
                {
                    outputLines.AppendFormat("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:"
                      , DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State);

                    foreach (var ve in eve.ValidationErrors)
                    {
                        outputLines.AppendFormat("- Property: \"{0}\", Error: \"{1}\""
                         , ve.PropertyName, ve.ErrorMessage);
                    }
                }

                throw new DbEntityValidationException(string.Format("Validation errors\r\n{0}"
                 , outputLines.ToString()), dbValEx);

            }
        }

        public DbContext Context
        {
            get { return _context; }
        }
        public GenericRepository<UserModel> UserRepository
        {
            get
            {

                if (this._userRepository == null)
                {
                    this._userRepository = new GenericRepository<UserModel>(_context);
                }
                return _userRepository;
            }
        }

        public GenericRepository<Consultation> ConsulationRepository
        {
            get
            {

                if (this._consultationRepository == null)
                {
                    this._consultationRepository = new GenericRepository<Consultation>(_context);
                }
                return _consultationRepository;
            }
        }

        public GenericRepository<Patient> PatientRepository
        {
            get
            {

                if (this._patientRepository == null)
                {
                    this._patientRepository = new GenericRepository<Patient>(_context);
                }
                return _patientRepository;
            }
        }

        public GenericRepository<Comment> CommentRepository
        {
            get
            {

                if (this._commentRepository == null)
                {
                    this._commentRepository = new GenericRepository<Comment>(_context);
                }
                return _commentRepository;
            }
        }

        public GenericRepository<Article> ArticleRepository
        {
            get
            {

                if (this._articleRepository == null)
                {
                    this._articleRepository = new GenericRepository<Article>(_context);
                }
                return _articleRepository;
            }
        }

        private bool _disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this._disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this._disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}