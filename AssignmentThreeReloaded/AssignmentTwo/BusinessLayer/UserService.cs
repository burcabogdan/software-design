﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;
using AssignmentTwo.Models;

namespace AssignmentTwo.Services
{
    public class UserService : IUserService
    {
        public GenericRepository<UserModel> _userRepository;
        public UnitOfWork UnitOfWork;

        public UserService()
        {
            UnitOfWork = new UnitOfWork();
            _userRepository = UnitOfWork.UserRepository;
        }
        public List<UserModel> GetUsers()
        {
            return _userRepository.Get().ToList();
        }

        public UserModel GetUserByUserName(string userName)
        {
            return _userRepository.Get().ToList().Find(t => t.UserName.Equals(userName));
        }

        public void InsertUserModel(UserModel user)
        {
            if (_userRepository.Get().ToList().Any(x => x.UserName.Equals(user.UserName) || x.Email.Equals(user.Email)))
                throw new PrimaryKeyException("Check that the username and the email address are unique.");
            _userRepository.Insert(user);
            UnitOfWork.Save();
        }

        public void EditUserModel(UserModel user)
        {

            if (_userRepository.Get().Count(x => x.Email.Equals(user.Email)) > 1)
                throw new PrimaryKeyException("Check that the username and the email address are unique.");
            var userT = _userRepository.GetById(user.UserName);
            userT.Name = user.Name;
            userT.Role = user.Role;
            userT.Password = user.Password;
            userT.Email = user.Email;
            _userRepository.Update(userT);
            UnitOfWork.Save();

        }

        public void DeleteUserModel(string userName)
        {
            _userRepository.Delete(userName);
            UnitOfWork.Save();
        }

        public UserModel ValidateUser(UserModel userModel)
        {
            var user = GetUserByUserName(userModel.UserName);
            if (user != null && user.Password.Equals(userModel.Password))
            {
                return user;
            }
            return null;

        }

    }
}