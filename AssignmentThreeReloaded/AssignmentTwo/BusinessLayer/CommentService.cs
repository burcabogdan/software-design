﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using SemesterosProjectos.Models;

namespace AssignmentTwo.BusinessLayer
{
    public class CommentService : ICommentService
    {
        public GenericRepository<Comment> _commentRepository;
        public UnitOfWork UnitOfWork;

        public CommentService()
        {
            UnitOfWork = new UnitOfWork();
            _commentRepository = UnitOfWork.CommentRepository;
        }

        public List<Comment> GetComments()
        {
            return _commentRepository.Get().ToList();
        }

        public List<Comment> GetCommentsByAuthor(string author)
        {
            return _commentRepository.Get().ToList().Where(t => t.CommenterId.Equals(author)).ToList();
        }

        public Comment GetCommentById(int id)
        {
            return _commentRepository.Get().ToList().Find(t => t.CommentId.Equals(id));
        }

        public void InsertComment(Comment Comment)
        {
            int xx = GetComments().Select(x => x.CommentId).OrderByDescending(t => t).FirstOrDefault();
            Comment.CommentId = xx++;
            _commentRepository.Insert(Comment);
            UnitOfWork.Save();
        }

        public void EditComment(Comment Comment)
        {
            if (_commentRepository.Get().Count(x => x.CommentId.Equals(Comment.CommentId)) > 1)
                throw new PrimaryKeyException();
            var CommentT = _commentRepository.GetById(Comment.CommentId);
            CommentT.CommentId = Comment.CommentId;
            CommentT.CommenterId = Comment.CommenterId;
            CommentT.ArticleId = Comment.ArticleId;
            CommentT.Content = Comment.Content;
            CommentT.IsFlagged = CommentT.IsFlagged;
            _commentRepository.Update(CommentT);
            UnitOfWork.Save();
        }

        public void DeleteComment(int CommentId)
        {
            _commentRepository.Delete(CommentId);
            UnitOfWork.Save();
        }

        public Comment ValidatComment(Comment Comment)
        {
            var user = GetCommentById(Comment.CommentId);
            var rx = new Regex("\\b(donkey|twerp|idiot)\\b", RegexOptions.IgnoreCase);
            return rx.IsMatch(Comment.Content) ? null : Comment;
        }

        public void FlagComment(int id)
        {
            var Comment = GetCommentById(id);
            var CommentT = _commentRepository.GetById(Comment.CommentId);
            CommentT.CommentId = Comment.CommentId;
            CommentT.CommenterId = Comment.CommenterId;
            CommentT.ArticleId = Comment.ArticleId;
            CommentT.Content = Comment.Content;
            CommentT.IsFlagged = true;
            _commentRepository.Update(CommentT);
            UnitOfWork.Save();
        }

        public List<Comment> GetCommentsByArticle(int articleID)
        {
            return _commentRepository.Get().ToList().Where(t => t.ArticleId.Equals(articleID)).ToList();
        }
    }
}
