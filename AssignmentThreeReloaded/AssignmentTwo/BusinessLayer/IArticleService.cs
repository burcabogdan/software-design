﻿using System.Collections.Generic;
using SemesterosProjectos.Models;

namespace SemesterosProjectos.BusinessLayer
{
    public interface IArticleService
    {
        List<Article> GetArticles();
        List<Article> GetArticlesByAuthor(string author);
        Article GetArticleById(int id);
        void InsertArticle(Article article);
        void EditArticle(Article article);
        void DeleteArticle(int articleId);
        Article ValidatArticle(Article article);
        void FlagArticle(int id);
    }
}