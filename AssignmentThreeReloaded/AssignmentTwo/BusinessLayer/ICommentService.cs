﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SemesterosProjectos.Models;

namespace AssignmentTwo.BusinessLayer
{
    public interface ICommentService
    {
        List<Comment> GetComments();
        List<Comment> GetCommentsByArticle(int articleID);
        List<Comment> GetCommentsByAuthor(string author);
        Comment GetCommentById(int id);
        void InsertComment(Comment Comment);
        void EditComment(Comment Comment);
        void DeleteComment(int CommentId);
        Comment ValidatComment(Comment Comment);
        void FlagComment(int id);

    }
}