﻿using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using AssignmentTwo;
using SemesterosProjectos.Models;

namespace SemesterosProjectos.BusinessLayer
{
    public class ArticleService : IArticleService
    {
        public GenericRepository<Article> _articleRepository;
        public UnitOfWork UnitOfWork;

        public ArticleService()
        {
            UnitOfWork = new UnitOfWork();
            _articleRepository = UnitOfWork.ArticleRepository;
        }

        public List<Article> GetArticles()
        {
            return _articleRepository.Get().ToList();
        }

        public List<Article> GetArticlesByAuthor(string author)
        {
            return _articleRepository.Get().ToList().Where(t => t.Writer.Equals(author)).ToList();
        }

        public Article GetArticleById(int id)
        {
            return _articleRepository.Get().ToList().Find(t => t.ArticleId.Equals(id));
        }

        public void InsertArticle(Article article)
        {
            if (_articleRepository.Get().ToList().Any(x => x.ArticleId.Equals(article.ArticleId)))
                throw new PrimaryKeyException();
            int xx = GetArticles().Select(x => x.ArticleId).OrderByDescending(t => t).FirstOrDefault();
            article.ArticleId = xx++;
            _articleRepository.Insert(article);
            UnitOfWork.Save();
        }

        public void EditArticle(Article article)
        {
            if (_articleRepository.Get().Count(x => x.ArticleId.Equals(article.ArticleId)) > 1)
                throw new PrimaryKeyException();
            var articleT = _articleRepository.GetById(article.ArticleId);
            articleT.ArticleId = article.ArticleId;
            articleT.Writer = article.Writer;
            articleT.Content = article.Content;
            articleT.IsFlagged = article.IsFlagged;
            articleT.ShareCount = article.ShareCount;
            _articleRepository.Update(articleT);
            UnitOfWork.Save();
        }

        public void DeleteArticle(int articleId)
        {
            _articleRepository.Delete(articleId);
            UnitOfWork.Save();
        }

        public Article ValidatArticle(Article article)
        {
            var user = GetArticleById(article.ArticleId);
            var rx = new Regex("\\b(donkey|twerp|idiot)\\b", RegexOptions.IgnoreCase);
            return rx.IsMatch(article.Content) ? null : article;
        }

        public void FlagArticle(int id)
        {
            var article = GetArticleById(id);
            var articleT = _articleRepository.GetById(article.ArticleId);
            articleT.ArticleId = article.ArticleId;
            articleT.Writer = article.Writer;
            articleT.Content = article.Content;
            articleT.IsFlagged = true;
            _articleRepository.Update(articleT);
            UnitOfWork.Save();
        }
    }
}