﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssignmentTwo.Models;

namespace AssignmentTwo.Services
{
    public interface IUserService
    {
        List<UserModel> GetUsers();
        UserModel GetUserByUserName(string userName);
        void InsertUserModel(UserModel user);
        void EditUserModel(UserModel user);
        void DeleteUserModel(string userName);
        UserModel ValidateUser(UserModel userModel);
    }
}