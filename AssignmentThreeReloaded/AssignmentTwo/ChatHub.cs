﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace AssignmentTwo
{
    public class ChatHub : Hub
    {
        public Task Join()
        {
            return Groups.Add(Context.ConnectionId, "foo");
        }


        public void Send(string name, string message)
        {
            // Call the addNewMessageToPage method to update clients.
            Clients.All.addNewMessageToPage(name, message);
        }

        public void SendToDoctor(string name, string message)
        {
        }
    }
}