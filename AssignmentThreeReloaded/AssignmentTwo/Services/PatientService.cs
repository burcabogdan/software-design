﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssignmentTwo.Models;

namespace AssignmentTwo.Services
{
    public class PatientService : IPatientService
    {
        public readonly GenericRepository<Patient> _patientRepository;
        public UnitOfWork UnitOfWork;

        public PatientService()
        {
            UnitOfWork = new UnitOfWork();
            _patientRepository = UnitOfWork.PatientRepository;
        }
        public List<Patient> GetPatients()
        {
            return _patientRepository.Get().ToList();
        }

        public Patient GetPatientByPNC(string pnc)
        {
            return _patientRepository.Get().ToList().Find(t => t.PNC.Equals(pnc));
        }

        public void InsertPatient(Patient patient)
        {
            if (_patientRepository.Get().ToList().Any(x => x.PNC.Equals(patient.PNC) || x.IdentityCardNumber.Equals(patient.IdentityCardNumber)))
                throw new PrimaryKeyException("Check that the PNC and the Identity Card Number are unique.");
            _patientRepository.Insert(patient);
            UnitOfWork.Save();
        }

        public void EditPatient(Patient patient)
        {

            if (_patientRepository.Get().Count(x => x.PNC.Equals(patient.PNC)) > 1)
                throw new PrimaryKeyException("Check that the username and the email address are unique.");
            var userT = _patientRepository.GetById(patient.PNC);
            userT.Name = patient.Name;
            userT.Address = patient.Address;
            userT.DateOfBirth = patient.DateOfBirth;
            userT.IdentityCardNumber = patient.IdentityCardNumber;
            userT.PNC = patient.PNC;
            _patientRepository.Update(userT);
            UnitOfWork.Save();

        }

        public void DeletePatient(string userName)
        {
            _patientRepository.Delete(userName);
            UnitOfWork.Save();
        }

        public List<string> PatientsToTring()
        {
            var xx = GetPatients().ToList();
            var ret = new List<String>();
            foreach (var xxs in xx)
            {
                ret.Add($"Name {xxs.Name} PNC {xxs.PNC}");
            }
            return ret;
        }

        public List<string> PPatientsToTring(List<Patient> patients)
        {
            var xx = patients;
            var ret = new List<String>();
            foreach (var xxs in xx)
            {
                ret.Add($"Name {xxs.Name} PNC {xxs.PNC}");
            }
            return ret;
        }
    }
}