﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssignmentTwo.Models;

namespace AssignmentTwo.Services
{
    public class ConsultationService : IConsultationService
    {
        public readonly GenericRepository<Consultation> _consulationRepository;
        public UnitOfWork UnitOfWork;

        public ConsultationService()
        {
            UnitOfWork = new UnitOfWork();
            _consulationRepository = UnitOfWork.ConsulationRepository;
        }
        public List<Consultation> GetConsulations()
        {
            return _consulationRepository.Get().ToList();
        }
        public Consultation GetConsultationById(string id)
        {
            return _consulationRepository.Get().ToList().Find(t => t.ConsulationId.ToString().Equals(id));
        }

        public string GetNextId()
        {
            var ids = _consulationRepository.Get().Select(x => x.ConsulationId).ToList();
            var c = new List<int>();
            foreach (var id in ids)
            {
                int n;
                int.TryParse(id, out n);
                c.Add(n);
            }
            return (c.Max() + 1).ToString();
        }


        public List<Consultation> GetConsultationsByDoctorId(string id)
        {
            return _consulationRepository.Get().ToList().Where(t => t.DoctorID.Equals(id)).ToList();
        }

        public List<Consultation> GetConsultationByPatientId(string id)
        {
            return _consulationRepository.Get().ToList().Where(t => t.PatientPNC.Equals(id)).ToList();
        }

        public void InsertConsultation(Consultation consultation)
        {
            if (_consulationRepository.Get().ToList().Any(x => x.ConsulationId.Equals(consultation.ConsulationId)))
                throw new PrimaryKeyException("Check that the PNC and the Identity Card Number are unique.");
            var times = GetConsultationsByDoctorId(consultation.DoctorID);

            foreach (var time in times)
            {
                TimeSpan timespan = time.EndDateTime.Subtract(time.StartDateTime);

            }
            _consulationRepository.Insert(consultation);
            UnitOfWork.Save();
        }

        public void EditConsultation(Consultation consultation)
        {
            //check dates
            var userT = _consulationRepository.GetById(consultation.ConsulationId);
            userT.ConsulationId = consultation.ConsulationId;
            userT.DoctorID = consultation.DoctorID;
            userT.StartDateTime = consultation.StartDateTime;
            userT.EndDateTime = consultation.EndDateTime;
            userT.PatientPNC = consultation.PatientPNC;
            userT.Diagnostic = consultation.Diagnostic ?? String.Empty;
            _consulationRepository.Update(userT);
            UnitOfWork.Save();

        }

        public void DeleteConsultation(string userName)
        {
            _consulationRepository.Delete(userName);
            UnitOfWork.Save();
        }

        public bool ValidateConsultation(string doctorId, DateTime start, DateTime end)
        {
            var times = GetConsultationsByDoctorId(doctorId).ToList();
            times.Sort((x, y) => y.StartDateTime.CompareTo(x.StartDateTime));
            times.Reverse();

     //       if (start.Hour < 8 || start.Hour > 17 || end.Hour < 8 || start.Hour < 17)
       //         return false;
            if (start.CompareTo(end) == 1)
                return false;
            var firstStartGreater = times.FirstOrDefault(x => x.StartDateTime.CompareTo(start) > 0);

            if (firstStartGreater == null)
                return true;
            if (firstStartGreater.EndDateTime.CompareTo(end) > 0 && firstStartGreater.StartDateTime.CompareTo(start) < 0)
                return false;

            var timesE = GetConsultationsByDoctorId(doctorId).ToList();
            timesE.Reverse();
            timesE.Sort((x, y) => y.EndDateTime.CompareTo(x.EndDateTime));
            var firstEndLessThan = timesE.FirstOrDefault(x => x.EndDateTime.CompareTo(end) < 0);
            if (firstEndLessThan != null && firstEndLessThan.StartDateTime.CompareTo(end) > 0)
                return false;
            return true;
        }
    }
}