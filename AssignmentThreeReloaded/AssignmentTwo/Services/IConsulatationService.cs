﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssignmentTwo.Models;

namespace AssignmentTwo.Services
{
    public interface IConsultationService
    {
        List<Consultation> GetConsulations();
        List<Consultation> GetConsultationsByDoctorId(string userName);
        List<Consultation> GetConsultationByPatientId(string userName);
        Consultation GetConsultationById(string userName);
        string GetNextId();
        void InsertConsultation(Consultation user);
        void EditConsultation(Consultation user);
        void DeleteConsultation(string userName);
        bool ValidateConsultation(string doctorId, DateTime start, DateTime end);
    }
}