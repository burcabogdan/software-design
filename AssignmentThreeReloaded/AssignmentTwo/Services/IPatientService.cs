﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssignmentTwo.Models;

namespace AssignmentTwo.Services
{
    public interface IPatientService
    {
        List<Patient> GetPatients();
        Patient GetPatientByPNC(string userName);
        void InsertPatient(Patient user);
        void EditPatient(Patient user);
        void DeletePatient(string userName);
        List<String> PatientsToTring();
        List<String> PPatientsToTring(List<Patient> patients);

    }

}