﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AssignmentTwo;
using AssignmentTwo.Models;
using System.Data;

namespace UnitTestProject2
{
        public class TestUnitOfWork : IDisposable
        {
            //here
            private readonly TestDbContetx _context = new TestDbContetx();
            private TestGenericRepository<Patient> _patientRepository;
            private TestGenericRepository<Consultation> _consultationRepository;
            private TestGenericRepository<UserModel> _userRepository;


            public void Save()
            {
                try
                {
                    _context.SaveChanges();
                }
                catch (System.Data.Entity.Validation.DbEntityValidationException dbValEx)
                {
                    var outputLines = new StringBuilder();
                    foreach (var eve in dbValEx.EntityValidationErrors)
                    {
                        outputLines.AppendFormat("{0}: Entity of type \"{1}\" in state \"{2}\" has the following validation errors:"
                          , DateTime.Now, eve.Entry.Entity.GetType().Name, eve.Entry.State);

                        foreach (var ve in eve.ValidationErrors)
                        {
                            outputLines.AppendFormat("- Property: \"{0}\", Error: \"{1}\""
                             , ve.PropertyName, ve.ErrorMessage);
                        }
                    }

                    throw new DbEntityValidationException(string.Format("Validation errors\r\n{0}"
                     , outputLines.ToString()), dbValEx);

                }
            }

            public DbContext Context
            {
                get { return _context; }
            }
            public TestGenericRepository<UserModel> UserRepository
            {
                get
                {

                    if (this._userRepository == null)
                    {
                        this._userRepository = new TestGenericRepository<UserModel>(_context);
                    }
                    return _userRepository;
                }
            }

            public TestGenericRepository<Consultation> ConsulationRepository
            {
                get
                {

                    if (this._consultationRepository == null)
                    {
                        this._consultationRepository = new TestGenericRepository<Consultation>(_context);
                    }
                    return _consultationRepository;
                }
            }

            public TestGenericRepository<Patient> PatientRepository
            {
                get
                {

                    if (this._patientRepository == null)
                    {
                        this._patientRepository = new TestGenericRepository<Patient>(_context);
                    }
                    return _patientRepository;
                }
            }

            private bool _disposed = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!this._disposed)
                {
                    if (disposing)
                    {
                        _context.Dispose();
                    }
                }
                this._disposed = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
        }
    }