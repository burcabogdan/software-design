﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject2
{
    [TestClass]
    public class UnitTest1
    {
        public AssignmentTwo.Services.UserService userService = new AssignmentTwo.Services.UserService();
        //public AssignmentTwo.BusinessLayer.ar.UserService userService = new AssignmentTwo.Services.UserService();

        [TestMethod]
        public void TestGetUsers()
        {
            //Arrange
            
            //Act
            var result = userService.GetUsers();
            //Assert

            Assert.IsTrue(result.Count >= 0);
        }

        //[TestMethod]
        //public void TestGetPatient()
        //{
        //    //Arrange

        //    //Act
        //    var result = patientService.GetPatients();
        //    //Assert

        //    Assert.IsTrue(result.Count > 0);
        //}


        //[TestMethod]
        //public void TestGetConsulatation()
        //{
        //    //Arrange

        //    //Act
        //    var result = consulationService.GetConsulations();
        //    //Assert

        //    Assert.IsTrue(result.Count > 0);
        //}

    }
}
