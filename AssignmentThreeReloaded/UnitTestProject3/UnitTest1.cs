﻿using System;
using AssignmentTwo.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SemesterosProjectos.BusinessLayer;

namespace UnitTestProject3
{
    [TestClass]
    public class UnitTest1
    {
        public UserService userService = new UserService();
        public ArticleService ArticleService = new ArticleService();

        [TestMethod]
        public void TestGetUsers()
        {
            //Arrange

            //Act
            var result = userService.GetUsers();
            //Assert

            Assert.IsTrue(result.Count > 0);
        }

        [TestMethod]
        public void TestGetArticles()
        {
            //Arrange

            //Act
            var result = ArticleService.GetArticles();

            //Assert
            Assert.IsTrue(result.Count > 0);
        }

    }
}
